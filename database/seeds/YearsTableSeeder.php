<?php
use Illuminate\Database\Seeder;

class YearsTableSeeder extends Seeder {
    public function run ()
    {
        DB::table('years')->insert([
            'label' => 'An universitar',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
    }
}

?>