<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollegeYearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('college_years', function (Blueprint $table) {
            $table->increments('id');
            $table->string('label');
            $table->unsignedInteger('year_id')->nullable();
            $table->timestamps();

            $table->foreign('year_id')->references('id')->on('years')->onDelete('cascade');
        });
    }

    public function year() {
        return $this->belongsTo(Year::class);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('college_years');
    }
}
