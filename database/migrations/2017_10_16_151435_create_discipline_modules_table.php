<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisciplineModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discipline_modules', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('teacher_id')->nullable();
            $table->unsignedInteger('discipline_id');
            $table->unsignedInteger('module_id')->nullable();
            $table->unsignedInteger('college_year_id')->nullable();
            $table->unsignedInteger('group_id')->nullable();
            $table->unsignedInteger('subgroup_id')->nullable();
            $table->unsignedInteger('rotation_id')->nullable();
            $table->timestamps();

            $table->foreign('teacher_id')->references('id')->on('teachers');
            $table->foreign('discipline_id')->references('id')->on('disciplines');
            $table->foreign('module_id')->references('id')->on('modules');
            $table->foreign('college_year_id')->references('id')->on('college_years');
            $table->foreign('group_id')->references('id')->on('groups');
            $table->foreign('subgroup_id')->references('id')->on('subgroups');
            $table->foreign('rotation_id')->references('id')->on('rotations');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discipline_modules');
    }
}
