<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Year extends Model
{
    protected $fillable = ['label', 'start', 'end'];

    public function inputs($request)
    {
        $this->label = $request->input('label');
        if ($request->has('start')){
            $this->start = $request->input('start');
        }
        if ($request->has('end')){
            $this->end = $request->input('end');
        }
    }

    public function saveYear($request)
    {
        $this->inputs($request);

        return $this->save();
    }

    public function updateYear($request)
    {
        $this->inputs($request);

        return $this->update();
    }

    public function collegeYears() {
        return $this->hasMany(CollegeYear::class);
    }

}
