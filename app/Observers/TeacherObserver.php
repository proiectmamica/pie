<?php
/**
 * Created by PhpStorm.
 * User: iuliana
 * Date: 31-Oct-17
 * Time: 10:56 PM
 */

namespace App\Observers;


use App\Mail\UserCreated;
use App\Teacher;
use App\User;
use Illuminate\Support\Facades\Mail;

class TeacherObserver
{

    public function created(Teacher $teacher)
    {
        $firstName = explode(" ", $teacher->first_name);
        $email = strtolower($firstName[0]).'.'.strtolower($teacher->last_name).'@ugal.ro';
        $password = str_random(6);
        User::create([
            'username' => $teacher->first_name. ' '. $teacher->last_name,
            'email' => $email,
            'password' => bcrypt($password),
            'right_id' => 2,
            'teacher_id' => $teacher->id
        ]);

        Mail::to($email)
            ->queue(new UserCreated($email, $password));
    }

}