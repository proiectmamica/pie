<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Week extends Model
{
    protected $id;
    protected $label;
    protected $start_week;
    protected $end_week;
    protected $week_type;

    protected $fillable = ['label', 'start', 'end', 'week_type', 'term_id'];

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getStartWeek()
    {
        return $this->start_week;
    }

    /**
     * @param mixed $start_week
     */
    public function setStartWeek($start_week)
    {
        $this->start_week = $start_week;
    }

    /**
     * @return mixed
     */
    public function getEndWeek()
    {
        return $this->end_week;
    }

    /**
     * @param mixed $end_week
     */
    public function setEndWeek($end_week)
    {
        $this->end_week = $end_week;
    }

    /**
     * @return mixed
     */
    public function getWeekType()
    {
        return $this->week_type;
    }

    /**
     * @param mixed $week_type
     */
    public function setWeekType($week_type)
    {
        $this->week_type = $week_type;
    }




    public function inputs($request)
    {
        $this->label = $request->input('label');
        $this->start = $request->input('start');
        $this->end = $request->input('end');
        $this->term_id = $request->input('term');
        $this->week_type = $request->input('week_type');
    }

    public function saveWeek($request) {
        $this->inputs($request);

        return $this->save();
    }

    public function updateWeek($request) {
        $this->inputs($request);

        return $this->update();
    }

    public function term()
    {
        return $this->belongsTo(Term::class);
    }

    public function disciplineModuleWeeks() {
        return $this->hasMany(DisciplineModuleWeek::class);
    }
}
