<?php

namespace App\Providers;

use App\Observers\TeacherObserver;
use App\Teacher;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Laravel\Dusk\DuskServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Teacher::observe(TeacherObserver::class);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(\Staudenmeir\DuskUpdater\DuskServiceProvider::class);
        $this->app->register(DuskServiceProvider::class);
    }
}
