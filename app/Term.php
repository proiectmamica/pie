<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
    protected $fillable = ['label', 'start', 'end', 'college_year_id'];

    public function inputs($request)
    {
        $this->label = $request->input('label');
        if ($request->has('start')){
            $this->start = $request->input('start');
        }
        if ($request->has('end')){
            $this->end = $request->input('end');
        }
        $this->college_year_id = $request->input('college_year');
    }

    public function saveTerm($request)
    {
        $this->inputs($request);

        return $this->save();
    }

    public function updateTerm($request)
    {
        $this->inputs($request);

        return $this->update();
    }

    public function disciplines()
    {
        return $this->hasMany(Discipline::class);
    }

    public function collegeYear()
    {
        return $this->belongsTo(CollegeYear::class);
    }

    public function weeks()
    {
        return  $this->hasMany(Week::class);
    }
}
