<?php
/**
 * Created by PhpStorm.
 * User: iuliana
 * Date: 31-Oct-17
 * Time: 9:12 PM
 */

use App\DisciplineModule;
use App\DisciplineModuleWeek;
use App\Student;

function saveFile($uploadFile, $path){
    $file = $uploadFile;
    $serverPath = public_path('excel');
    if(!is_dir($serverPath))
    {
        mkdir($serverPath, 0777, true);
    }
    $file->move($serverPath,$file->getClientOriginalName());

    return $serverPath.'\\'.$file->getClientOriginalName();

}

function hasAttendance($id, $weekdiscipline) {
    $student = Student::find($id);
    $attendance = $student->attendances()->where('discipline_module_week_id', $weekdiscipline)->first ();
    if ($attendance) {
        return true;
    } else {
        return false;
    }
}

function  getOperated($discipline, $module){
    $operated = DisciplineModuleWeek::where('status_id', 2)->whereIn('discipline_module_id', DisciplineModule::where('discipline_id', $discipline->id)->where('module_id', $module)->pluck('id')->toArray())->count();
    return $operated;
}

function getNumberOfWeeks($hours){
    return $hours % 2 == 0 ? 14 : 7;
}