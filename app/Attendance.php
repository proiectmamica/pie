<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    public function inputs($request)
    {
        $this->student_id = $request->input('student_id');

        $this->discipline_module_week_id = $request->input('discipline_module_week_id');



    }

    public function saveAttendance($request)
    {
        $this->inputs($request);

        return $this->save();
    }

    public function updateAttendance($request)
    {
        $this->inputs($request);

        return $this->update();
    }
    public function student() {
        return $this->belongsTo(Student::class);
    }
    public function discipline_module_week() {
        return $this->belongsTo(DisciplineModuleWeek::class);
    }
}
