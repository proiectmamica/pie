<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{

    protected $fillable = ['first_name', 'last_name', 'subgroup_id'];


    public function inputs($request)
    {
            $this->first_name = $request->input('first_name');
            $this->last_name = $request->input('last_name');
            $this->subgroup_id = $request->input('subgroup');
    }

    public function saveStudent($request) {
        $this->inputs($request);

        return $this->save();
    }

    public function updateStudent($request) {
        $this->inputs($request);

        return $this->update();
    }

    public function subgroup()
    {
        return $this->belongsTo(Subgroup::class);
    }

    public function attendances () {
        return $this->hasMany(Attendance::class);
    }

}
