<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    public function inputs($request)
    {
        $this->label = $request->input('label');

    }

    public function saveStatus($request)
    {
        $this->inputs($request);

        return $this->save();
    }

    public function updateStatus($request)
    {
        $this->inputs($request);

        return $this->update();
    }

    public function discipline_module_weeks() {
        return $this->hasMany(DisciplineModuleWeek::class);
    }
}
