<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{

    protected $fillable = ['label', 'college_year_id'];

    public function inputs($request)
    {
            $this->label = $request->input('label');
            $this->college_year_id = $request->input('college_year');
    }

    public function saveGroup($request) {
        $this->inputs($request);

        return $this->save();
    }

    public function updateGroup($request) {
        $this->inputs($request);

        return $this->update();
    }

    public function collegeYear()
    {
        return $this->belongsTo(CollegeYear::class);
    }

    public function subgroups()
    {
        return $this->hasMany(Subgroup::class);
    }
    public function discipline_modules() {
        return $this->hasMany(DisciplineModule::class);
    }

    public function students()
    {
        return $this->hasManyThrough(Student::class, Subgroup::class);
    }

}
