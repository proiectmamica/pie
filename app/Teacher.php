<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $fillable = ['first_name', 'last_name'];

    public function inputs($request)
    {
        $this->first_name = $request->input('first_name');
        $this->last_name = $request->input('last_name');
    }

    public function saveTeacher($request)
    {
        $this->inputs($request);

        return $this->save();
    }

    public function updateTeacher($request)
    {
        $this->inputs($request);

        return $this->update();
    }
    public function discipline_modules() {
        return $this->hasMany(DisciplineModule::class);
    }
}
