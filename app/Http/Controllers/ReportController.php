<?php

namespace App\Http\Controllers;

use App\Discipline;
use App\DisciplineModule;
use App\DisciplineModuleWeek;
use App\Student;
use App\Term;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PDF;
class ReportController extends Controller
{
    public function index()
    {

        $disciplines = Discipline::all();
        $students = Student::orderBy('last_name', 'ASC')->get();


        return view('reports.index')
            ->with('disciplines', $disciplines)
            ->with('students' , $students)
            ;
    }

    public function total($id)
    {
        ini_set('max_execution_time', 600);

        $discipline = Discipline::find($id);

        $courses  = $discipline->discipline_modules()->where('module_id', 1)->get();
        if ($courses->count() > 0) {
            $courseAttendance = 0;
            $operated = getOperated($discipline, 1);
            foreach ($courses as $course) {
                $courseAttendance += $course->attendances()->count();
            }
            $courseStudents = 0;
            foreach ($discipline->term->collegeYear->groups as $group) {
//            $courseStudents += $group->students()->count() * (($discipline->number_hours_course % 2 == 0) ? 14 : 7) ;
                $courseStudents += $group->students()->count() * $operated;

            }
        }


        $seminaries  = $discipline->discipline_modules()->where('module_id', 3)->get();
        if ($seminaries->count() > 0){
            $seminaryAttendance = 0;
            $seminaryStudents = 0;
            $operated = getOperated($discipline, 3);

            foreach ($seminaries as $seminary){
                $seminaryAttendance += $seminary->attendances()->count();
                $seminaryStudents += $seminary->group->students->count() * $operated;
            }
        }
        $laboratories  = $discipline->discipline_modules()->where('module_id', 2)->get();
        if ($laboratories->count() > 0){
            $laboratoryAttendance = 0;
            $laboratoryStudents = 0;
            $operated = getOperated($discipline, 2);

            foreach ($laboratories as $laboratory){
                $laboratoryAttendance += $laboratory->attendances()->count();
                $laboratoryStudents += $laboratory->subgroupStudents()->count() *  $operated;
            }
        }


        $projects  = $discipline->discipline_modules()->where('module_id', 4)->get();
        if ($projects->count() > 0){
            $projectAttendance = 0;
            $projectStudents = 0;
            $operated = getOperated($discipline, 4);

            foreach ($projects as $project){
                $projectAttendance += $project->attendances()->count();
                $projectStudents += $project->group->students->count() *  $operated;

            }
        }


        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('reports.total_attendances', [
            'discipline' => $discipline,
            'course' => isset($courseAttendance) ? $courseAttendance : null,
            'seminary' => isset($seminaryAttendance) ? $seminaryAttendance : null,
            'laboratory' => isset($laboratoryAttendance) ? $laboratoryAttendance : null,
            'project' => isset($projectAttendance) ? $projectAttendance : null,
            'courseStudents' => isset($courseStudents) ? $courseStudents : null,
            'seminaryStudents' => isset($seminaryStudents) ? $seminaryStudents : null ,
            'laboratoryStudents' => isset($laboratoryStudents) ? $laboratoryStudents : null,
            'projectStudents' => isset($projectStudents) ? $projectStudents : null,

        ]);

//        return view('pdf.invoice');
        return $pdf->stream("Prezente totale.pdf");
    }

    public function groups($id)
    {
        ini_set('max_execution_time', 600);

        $discipline = Discipline::find($id);
        $groups = $discipline->term->collegeYear->groups;

        if ($discipline->number_hours_course > 0) {

            $courses = $discipline->discipline_modules()->where('module_id', 1)->get();
            $groupCourses = [];
            foreach ($courses as $course) {
                foreach ($groups as $group) {
                    $attendances = $course->attendances()->whereHas('student', function ($q) use ($group) {
                        return $q->whereIn('subgroup_id', $group->subgroups()->pluck('id')->toArray());
                    })->count();
                    $operated = getOperated($discipline, 1);

                    $students = $group->students()->count() * $operated;

                    $groupCourses[$group->label] = [
                        'attendances' => $attendances,
                        'total' => $students
                    ];

                }
            }
        }else{
            $groupCourses = null;
        }

        if ($discipline->number_hours_seminary > 0) {

            $seminaries = $discipline->discipline_modules()->where('module_id', 3)->get();
            $groupSeminaries = [];
            foreach ($seminaries as $seminary) {
                foreach ($groups as $group) {
                    $attendances = $seminary->attendances()->whereHas('student', function ($q) use ($group) {
                        return $q->whereIn('subgroup_id', $group->subgroups()->pluck('id')->toArray());
                    })->count();

                    $students = $seminary->group->students->count() * getOperated($discipline, 3);

                    $groupSeminaries[$group->label] = [
                        'attendances' => $attendances,
                        'total' => $students
                    ];
                }
            }
        }else{
            $groupSeminaries = null;
        }

        if ($discipline->number_hours_laboratory > 0){
            $laboratories  = $discipline->discipline_modules()->where('module_id', 2)->get();
            $groupLaboratories = [];
            foreach ($laboratories as $laboratory){
                foreach ($groups as $group){
                    $attendances = $laboratory->attendances()->whereHas('student', function ($q) use ($group){
                        return $q->whereIn('subgroup_id', $group->subgroups()->pluck('id')->toArray());
                    })->count();

                    $students = $laboratory->subgroupStudents()->count() *  getOperated($discipline, 2);

                    $groupLaboratories[$group->label] = [
                        'attendances' => $attendances,
                        'total' => $students
                    ];

                }

            }
        }else{
            $groupLaboratories = null;
        }

        if ($discipline->number_hours_project > 0) {
            $projects  = $discipline->discipline_modules()->where('module_id', 4)->get();
            $groupProjects = [];
            foreach ($projects as $project){
                foreach ($groups as $group){
                    $attendances = $project->attendances()->whereHas('student', function ($q) use ($group){
                        return $q->whereIn('subgroup_id', $group->subgroups()->pluck('id')->toArray());
                    })->count();

                    $students = $project->group->students->count() *  getOperated($discipline, 4);

                    $groupProjects[$group->label] = [
                        'attendances' => $attendances,
                        'total' => $students
                    ];
                }

            }
        }else{
            $groupProjects = null;
        }



        $pdf = PDF::loadView('reports.group',[
            'discipline' => $discipline,
            'groups' => $groups,
            'course' => $groupCourses,
            'seminary' => $groupSeminaries,
            'laboratory' => $groupLaboratories,
            'project' => $groupProjects
        ]);

        return $pdf->stream();

    }

    public function students($id)
    {
        ini_set('max_execution_time', 600);

        $discipline = Discipline::find($id);
        $groups = $discipline->term->collegeYear->groups;
        $subgroups = [];
        foreach ($groups as $group){
            foreach ($group->subgroups as $subgroup)
                array_push($subgroups, $subgroup->id);
        }

        $students = Student::whereIn('subgroup_id', $subgroups)->get();

        if ($discipline->number_hours_course > 0) {
            $courses = $discipline->discipline_modules()->where('module_id', 1)->get();
            $studentCourses = [];
            foreach ($courses as $course) {
                foreach ($students as $student) {
                    $attendances = $course->attendances()->where('student_id', $student->id)->count();
                    $total =getOperated($discipline, 1);

                    $studentCourses[$student->id] = [
                        'attendances' => $attendances,
                        'total' => $total
                    ];

                }
            }
        }else{
            $studentCourses = null;
        }

        if ($discipline->number_hours_seminary > 0) {
            $seminaries = $discipline->discipline_modules()->where('module_id', 3)->get();
            $studentSeminaries = [];
            foreach ($seminaries as $seminary) {
                foreach ($students as $student) {

                    $attendances = $seminary->attendances()->where('student_id', $student->id)->count();
                    $total =getOperated($discipline, 3);


                    if(isset( $studentSeminaries[$student->id]))
                    {
                        $oldAttendances = $studentSeminaries[$student->id]['attendances'] + $attendances;
                    }
                    else
                        $oldAttendances = $attendances;


                    $studentSeminaries[$student->id] = [
                        'attendances' => $oldAttendances,
                        'total' => $total
                    ];
                }

            }
        }else{
            $studentSeminaries = null;
        }


        if ($discipline->number_hours_laboratory > 0){
            $laboratories  = $discipline->discipline_modules()->where('module_id', 2)->get();
            $studentLaboratories = [];
            foreach ($laboratories as $laboratory){
                foreach ($students as $student){
                    $attendances = $laboratory->attendances()->where('student_id', $student->id)->count();
                    $total =getOperated($discipline, 2);


                    if(isset( $studentLaboratories[$student->id]))
                    {
                        $oldAttendances = $studentLaboratories[$student->id]['attendances'] + $attendances;
                    }
                    else
                    {
                        $oldAttendances = $attendances;
                    }



                    $studentLaboratories[$student->id] = [
                        'attendances' => $oldAttendances,
                        'total' => $total
                    ];

                }

            }
        }else{
            $studentLaboratories = null;
        }

        if ($discipline->number_hours_project > 0) {
            $projects  = $discipline->discipline_modules()->where('module_id', 4)->get();
            $studentProjects = [];
            foreach ($projects as $project){
                foreach ($students as $student){
                    $attendances = $project->attendances()->where('student_id', $student->id)->count();
                    $total =getOperated($discipline, 4);


                    $studentProjects[$student->id] = [
                        'attendances' => $attendances,
                        'total' => $total
                    ];
                }

            }
        }else{
            $studentProjects = null;
        }

        $pdf = PDF::loadView('reports.students',[
            'discipline' => $discipline,
            'students' => $students,
            'course' => $studentCourses,
            'seminary' => $studentSeminaries,
            'laboratory' => $studentLaboratories,
            'project' => $studentProjects
        ]);

        return $pdf->stream("Prezenta studenti ".$discipline->name.".pdf");

    }

    public function student($id)
    {
        ini_set('max_execution_time', 600);

        $student = Student::find($id);
//        get discipline module week ids
        $disciplineModuleWeekIds = $student->attendances()->pluck('discipline_module_week_id')->toArray();
//        get discipline modules ids
        $disciplineModuleIds = DisciplineModuleWeek::whereIn('id', $disciplineModuleWeekIds)->pluck('discipline_module_id')->toArray();
//        disciplines
        $disciplines = Discipline::whereIn('id', DisciplineModule::whereIn('id', $disciplineModuleIds)->pluck('discipline_id')->toArray())->get();
        $courseAttendances = [];
        $seminaryAttendance = [];
        $laboratoryAttendance = [];
        $projectAttendance = [];

        foreach ($disciplines as $discipline){
            if ($discipline->number_hours_course > 0){
                $courses =  $discipline->discipline_modules()->where('module_id', 1)->get();

                foreach ($courses as $course){
                    $attendances = $course->attendances()->where('student_id', $student->id)->count();
                    $total = getOperated($discipline, 1);
                    $courseAttendances[$discipline->id] = [
                        'attendances' => $attendances,
                        'total' => $total
                    ];
                }
            }

            if ($discipline->number_hours_seminary > 0){
                $seminaries = $discipline->discipline_modules()->where('module_id', 3)->get();
                foreach ($seminaries as $seminary){
                    $attendances = $seminary->attendances()->where('student_id', $student->id)->count();
                    $total =getOperated($discipline, 3);


                    if(isset( $seminaryAttendance[$discipline->id]))
                    {
                        $oldAttendances = $seminaryAttendance[$discipline->id]['attendances'] + $attendances;
                    }
                    else
                        $oldAttendances = $attendances;


                    $seminaryAttendance[$discipline->id] = [
                        'attendances' => $oldAttendances,
                        'total' => $total
                    ];
                }
            }
            if ($discipline->number_hours_laboratory > 0){
                $laboratories  = $discipline->discipline_modules()->where('module_id', 2)->get();
                foreach ($laboratories as $laboratory){
                    $attendances = $laboratory->attendances()->where('student_id', $student->id)->count();
                    $total =getOperated($discipline, 2);


                    if(isset( $laboratoryAttendance[$discipline->id]))
                    {
                        $oldAttendances = $laboratoryAttendance[$discipline->id]['attendances'] + $attendances;
                    }
                    else
                    {
                        $oldAttendances = $attendances;
                    }

                    $laboratoryAttendance[$discipline->id] = [
                        'attendances' => $oldAttendances,
                        'total' => $total
                    ];

                }

            }

            if ($discipline->number_hours_project > 0){
                $projects  = $discipline->discipline_modules()->where('module_id', 4)->get();

                foreach ($projects as $project){
                    $attendances = $project->attendances()->where('student_id', $student->id)->count();
                    $total =getOperated($discipline, 4);
                    if(isset( $projectAttendance[$discipline->id]))
                    {
                        $oldAttendances = $projectAttendance[$discipline->id]['attendances'] + $attendances;
                    }
                    else
                    {
                        $oldAttendances = $attendances;
                    }

                    $projectAttendance[$discipline->id] = [
                        'attendances' => $oldAttendances,
                        'total' => $total
                    ];

                }

            }
//            $disciplineMOduleWeeks = DisciplineModuleWeek::whereIn('discipline_module_id', DisciplineModule::where('discipline_id', $discipline->id)->pluck('id')->toArray())->get();

        }
        $pdf = PDF::loadView('reports.student',[
            'disciplines' => $disciplines,
            'student' => $student,
            'course' => isset($courseAttendances) ? $courseAttendances : null,
            'seminary' => isset($seminaryAttendance) ? $seminaryAttendance : null,
            'laboratory' => isset($laboratoryAttendance) ? $laboratoryAttendance : null,
            'project' => isset($projectAttendance) ? $projectAttendance : null
        ]);

        return $pdf->stream("Prezenta ".$student->last_name." ".$student->first_name.".pdf");

    }

    public function weeks($id)
    {
        ini_set('max_execution_time', 600);

        $student = Student::find($id);

        $today = Carbon::now();
        $term = Term::whereDate('start', '<=', $today->toDateString())->whereDate('end', '>=', $today->toDateString())->first();

        $student = Student::find($id);
//        get discipline module week ids
        $disciplineModuleWeekIds = $student->attendances()->pluck('discipline_module_week_id')->toArray();
//        get discipline modules ids
        $disciplineModuleIds = DisciplineModuleWeek::whereIn('id', $disciplineModuleWeekIds)->pluck('discipline_module_id')->toArray();
//        disciplines
        $disciplines = Discipline::whereIn('id', DisciplineModule::whereIn('id', $disciplineModuleIds)->pluck('discipline_id')->toArray())->get();
        $courseAttendances = [];
        $seminaryAttendance = [];
        $laboratoryAttendance = [];
        $projectAttendance = [];



        foreach ($term->weeks as $week){
            foreach ($disciplines as $discipline){
                if ($discipline->number_hours_course > 0){
                    $courses =  $discipline->discipline_modules()->where('module_id', 1)->get();
                    $disciplineModuleWeeks = DisciplineModuleWeek::whereIn('discipline_module_id', $courses->pluck('id')->toArray())->where('week_id', $week->id)->get();
                    foreach ($courses as $course){
                        $attendances = $course->attendances()->where('student_id', $student->id)->whereIn('discipline_module_week_id', $disciplineModuleWeeks->pluck('id')->toArray())->count();
                        $total = getOperated($discipline, 1);
                        $courseAttendances[$discipline->id][$week->id] = [
                            'attendances' => $attendances,
                            'total' => $total
                        ];
                    }
                }

                if ($discipline->number_hours_seminary > 0){
                    $seminaries = $discipline->discipline_modules()->where('module_id', 3)->get();
                    $disciplineModuleWeeks = DisciplineModuleWeek::whereIn('discipline_module_id', $seminaries->pluck('id')->toArray())->where('week_id', $week->id)->get();
                    foreach ($seminaries as $seminary){
                        $attendances = $seminary->attendances()->where('student_id', $student->id)->whereIn('discipline_module_week_id', $disciplineModuleWeeks->pluck('id')->toArray())->count();
                        $total =getOperated($discipline, 3);

                        if(isset( $seminaryAttendance[$discipline->id][$week->id]))
                        {
                            $oldAttendances = $seminaryAttendance[$discipline->id][$week->id]['attendances'] + $attendances;
                        }
                        else
                            $oldAttendances = $attendances;


                        $seminaryAttendance[$discipline->id][$week->id] = [
                            'attendances' => $oldAttendances,
                            'total' => $total
                        ];
                    }
                }
                if ($discipline->number_hours_laboratory > 0){
                    $laboratories  = $discipline->discipline_modules()->where('module_id', 2)->get();
                    $disciplineModuleWeeks = DisciplineModuleWeek::whereIn('discipline_module_id', $laboratories->pluck('id')->toArray())->where('week_id', $week->id)->get();

                    foreach ($laboratories as $laboratory){
                        $attendances = $laboratory->attendances()->where('student_id', $student->id)->whereIn('discipline_module_week_id', $disciplineModuleWeeks->pluck('id')->toArray())->count();
                        $total =getOperated($discipline, 2);


                        if(isset( $laboratoryAttendance[$discipline->id][$week->id]))
                        {
                            $oldAttendances = $laboratoryAttendance[$discipline->id][$week->id]['attendances'] + $attendances;
                        }
                        else
                        {
                            $oldAttendances = $attendances;
                        }

                        $laboratoryAttendance[$discipline->id][$week->id] = [
                            'attendances' => $oldAttendances,
                            'total' => $total
                        ];

                    }

                }

                if ($discipline->number_hours_project > 0){
                    $projects  = $discipline->discipline_modules()->where('module_id', 4)->get();
                    $disciplineModuleWeeks = DisciplineModuleWeek::whereIn('discipline_module_id', $projects->pluck('id')->toArray())->where('week_id', $week->id)->get();

                    foreach ($projects as $project){
                        $attendances = $project->attendances()->where('student_id', $student->id)->whereIn('discipline_module_week_id', $disciplineModuleWeeks->pluck('id')->toArray())->count();
                        $total =getOperated($discipline, 4);
                        if(isset( $projectAttendance[$discipline->id][$week->id]))
                        {
                            $oldAttendances = $projectAttendance[$discipline->id][$week->id]['attendances'] + $attendances;
                        }
                        else
                        {
                            $oldAttendances = $attendances;
                        }

                        $projectAttendance[$discipline->id][$week->id] = [
                            'attendances' => $oldAttendances,
                            'total' => $total
                        ];

                    }

                }
//            $disciplineMOduleWeeks = DisciplineModuleWeek::whereIn('discipline_module_id', DisciplineModule::where('discipline_id', $discipline->id)->pluck('id')->toArray())->get();

            }
        }


        $pdf = PDF::loadView('reports.weeks',[
            'student' => $student,
            'weeks' => $term->weeks,
            'disciplines' => $disciplines,
            'course' => isset($courseAttendances) ? $courseAttendances : null,
            'seminary' => isset($seminaryAttendance) ? $seminaryAttendance : null,
            'laboratory' => isset($laboratoryAttendance) ? $laboratoryAttendance : null,
            'project' => isset($projectAttendance) ? $projectAttendance : null
        ]);

        return $pdf->stream("Prezenta pe saptamani".$student->last_name." ".$student->first_name.".pdf");

    }

}
