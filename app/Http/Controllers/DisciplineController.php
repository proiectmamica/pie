<?php

namespace App\Http\Controllers;

use App\Discipline;
use App\Term;
use Illuminate\Http\Request;

class DisciplineController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        view()->share('adminMenu', 'disciplines');
    }/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('discipline.index')
            ->with('disciplines', Discipline::orderBy('created_at', 'desc')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $terms = Term::all();
        return view('discipline.create')
            ->with('terms',$terms);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $discipline = new Discipline();
        $discipline ->saveDiscipline($request);

        return redirect()->route('discipline.index')->with('successes', ['Disciplina a fost salvata!']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $terms = Term::all();
        $discipline = Discipline::find($id);

//        testing
        return view('discipline.edit')->with('discipline', $discipline)
                                            ->with('terms',$terms);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $discipline = Discipline::find($id);
        $discipline->updateDiscipline($request);

        return redirect()->route('discipline.index')->with('successes', ['Disciplina a fost actualizata!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
