<?php

namespace App\Http\Controllers;

use App\Term;
use App\Week;
use Illuminate\Http\Request;

class WeekController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        view()->share('adminMenu', 'weeks');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('week.index')
            ->with('weeks', Week::orderBy('created_at', 'asc')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $terms =  Term::all();
        return view('week.create')
            ->with('terms', $terms);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $week = new Week();
        $week->saveWeek($request);

        return redirect()->route('week.index')->with('successes', ['Saptamana a fost introdusa!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $terms = Term::all();
        $week = Week::find($id);

        return view('week.edit')->with('week', $week)
                                      ->with('terms', $terms);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $week = Week::find($id);
        $week->updateWeek($request);

        return redirect()->route('week.index')->with('successes', ['Saptamana actualizata!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
