<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\DisciplineModule;
use App\DisciplineModuleWeek;
use App\Week;
use Illuminate\Http\Request;
use App\Discipline;
use App\Group;
use App\Subgroup;
use App\Teacher;
use App\Rotation;
use App\CollegeYear;
use App\Module;
use Illuminate\Support\Facades\Auth;
class DisciplineModuleController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        view()->share('adminMenu', 'discipline_modules');
    }/**
/**
 * Display a listing of the resource.
 *
 * @return \Illuminate\Http\Response
 */
    public function index()
    {
        return view('discipline_module.index')
            ->with('discipline_modules', DisciplineModule::orderBy('discipline_id', 'asc')->get());
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $disciplines = Discipline::all();
        $groups = Group::all();
        $rotations = Rotation::all();
        $subgroups = Subgroup::all();
        $teachers = Teacher::all();
        $college_years = CollegeYear::all();
        $modules = Module::all();
        return view('discipline_module.create')
             ->with('disciplines',$disciplines)
            ->with('groups',$groups)
            ->with('rotations',$rotations)
            ->with('subgroups',$subgroups)
            ->with('teachers',$teachers)
            ->with('college_years',$college_years)
            ->with('modules',$modules);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $discipline_module = new DisciplineModule();
        $discipline_module ->saveDisciplineModule($request);

        return redirect()->route('discipline_module.index')->with('successes', ['Modulul disciplinei a fost salvat!']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $discipline_module = DisciplineModule::find($id);
        $disciplines = Discipline::all();
        $groups = Group::all();
        $rotations = Rotation::all();
        $subgroups = Subgroup::all();
        $teachers = Teacher::all();
        $college_years = CollegeYear::all();
        $modules = Module::all();

//        testing
        return view('discipline_module.edit')
            ->with('discipline_module', $discipline_module)
            ->with('disciplines',$disciplines)
            ->with('groups',$groups)
            ->with('rotations',$rotations)
            ->with('subgroups',$subgroups)
            ->with('teachers',$teachers)
            ->with('college_years',$college_years)
            ->with('modules',$modules);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $discipline_module = DisciplineModule::find($id);
        $discipline_module->updateDisciplineModule($request);

//        Pentru DisciplineModule inseram in DisciplineModuleWeek cate o intrare pentru fiecare saptamana in care se tine modulul
//        Daca e pe semestrul 1 punem doar saptamana din semestrul 1


        DisciplineModuleWeek::where('discipline_module_id', $discipline_module->id)->delete();


        $this->createWeeks($discipline_module);

        return redirect()->route('discipline_module.index')->with('successes', ['Modulul disciplinei a fost actualizat!']);
    }

    protected  function createWeeks($discipline_module){
        $disciplineTerm = $discipline_module->discipline->term_id;
        if ($disciplineTerm % 2 == 0){
            $term = 2;
        }else{
            $term = 1;
        }

        $weeks = Week::where('term_id',$term)->where('week_type', 'activitate')->get();

        if ($discipline_module->rotation_id == 1){
            foreach ($weeks as $week){
                $disciplineModuleWeek = new DisciplineModuleWeek();
                $disciplineModuleWeek->saveDisciplineModuleWeek($discipline_module->id, $week->id, 1);
            }
        }elseif($discipline_module->rotation_id == 2){
            for ($i = 0; $i < $weeks->count(); $i += 2){
                $disciplineModuleWeek = new DisciplineModuleWeek();
                $disciplineModuleWeek->saveDisciplineModuleWeek($discipline_module->id, $weeks[$i]->id, 1);
            }
        }elseif($discipline_module->rotation_id == 3){
            for ($i = 1; $i < $weeks->count(); $i += 2){
                $disciplineModuleWeek = new DisciplineModuleWeek();
                $disciplineModuleWeek->saveDisciplineModuleWeek($discipline_module->id, $weeks[$i]->id, 1);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function assign()
    {
        return view('discipline_module.assign')
            ->with('disciplineModules', DisciplineModule::whereNull('teacher_id')->whereNull('rotation_id')->orderBy('discipline_id', 'asc')->get())
            ->with('teachers', Teacher::orderBy('last_name', 'asc')->get())
            ->with('rotations', Rotation::all());
    }

    public function saveAssign(Request $request)
    {
        foreach ($request->input('discipline_modules') as $disciplineModuleId){
            $disciplineModule = DisciplineModule::find($disciplineModuleId);
            $disciplineModule->teacher_id = $request->input('teacher');
            $disciplineModule->rotation_id = $request->input('rotation-'.$disciplineModuleId);
            $disciplineModule->update();

            $this->createWeeks($disciplineModule);

        }

        return response()->json($request->input('discipline_modules'));
    }

    protected function operatedModules($moduleId) {
        $modulesOperated = DisciplineModuleWeek::whereHas('disciplineModule', function ($q) use ($moduleId) {
            return $q->where('teacher_id', Auth::user()->teacher_id)->where('module_id' ,$moduleId)->where('status_id',2);
        })->get();
        return $modulesOperated;
    }
    public function getOperatedModules() {
        $courses =  $this->operatedModules(1);
        $laboratories =  $this->operatedModules( 2);
        $seminaries = $this->operatedModules(3);
        $projects =  $this->operatedModules(4);
        return view('discipline_module.operated')
            ->with('courses', $courses)
            ->with('laboratories', $laboratories)
            ->with('seminaries', $seminaries)
            ->with('projects',$projects);
    }
}
