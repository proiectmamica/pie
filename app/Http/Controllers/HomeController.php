<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\CollegeYear;
use App\Discipline;
use App\DisciplineModule;
use App\DisciplineModuleWeek;
use App\Group;
use App\Student;
use App\Subgroup;
use App\Teacher;
use App\Week;
use App\Year;
use App\Term;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Artisan;
use Maatwebsite\Excel\Facades\Excel;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($weekId = null)
    {
        if(Auth::user()->right_id == 1)
            return view('dashboard.administrator');
        else
        {
            $today = Carbon::now();
            if (isset($weekId))
                $week = Week::find($weekId);
            else if(!is_null(\request('week')))
                $week = Week::find(\request('week'));
            else
                $week = Week::whereDate('start', '<=', $today->toDateString())->whereDate('end', '>=', $today->toDateString())->first();
            $next = Week::where('id', '>', $week->id)->orderBy('id','asc')->first();
            $prev = Week::where('id', '<', $week->id)->orderBy('id','desc')->first();


//            cursuri
            $courses =  $this->getWeekModules($week, 1);
//        laboratoare
            $laboratories =  $this->getWeekModules($week, 2);
//          seminare
            $seminaries = $this->getWeekModules($week, 3);
//          proiecte
            $projects =  $this->getWeekModules($week, 4);

            $teacher = null;
            if (!is_null(\request('teacher')))
                $teacher = Teacher::find(\request('teacher'));
            return view('dashboard.teacher')
                ->with('week',$week)
                ->with('courses',$courses)
                ->with('laboratories', $laboratories)
                ->with('seminaries', $seminaries)
                ->with('projects',$projects)
                ->with('next', $next)
                ->with('prev', $prev)
                ->with('teachers', Teacher::where('id', '<>' , Auth::user()->teacher_id)->orderBy('first_name', 'ASC')->get())
                ->with('weeks', Week::all())
                ->with('teacher', $teacher)
                ;
        }
    }

    protected  function getWeekModules($week, $moduleId){
        $teacherId = Auth::user()->teacher_id;
        if (!is_null(\request('teacher'))){
            $teacherId = \request('teacher');
        }
        $modules =  DisciplineModuleWeek::where('week_id', $week->id)->whereHas('disciplineModule', function ($q) use ($moduleId, $teacherId) {
            return $q->where('teacher_id', $teacherId)->where('module_id' ,$moduleId)->where('status_id',1);
        })->get();
        return $modules;
    }

    public function import(Request $request)
    {
        ini_set('max_execution_time', 600);
        $this->emptyTables();
        $file = saveFile($request->file('file'), 'excel');
//        Years
        $data = $this->loadSheet('Years', $file);
        if(!empty($data) && $data->count()){
            foreach ($data as $key => $value) {
                Year::create([
                    'label' => $value->denumire,
                    'start' => $value->inceput,
                    'end' => $value->sfarsit,
                ]);
            }
        }

//        Terms
        $data = $this->loadSheet('Terms', $file);
        if(!empty($data) && $data->count()){
            foreach ($data as $key => $value) {
                $collegeYear = CollegeYear::where('label', $value->anul)->first();

                if (!is_null($collegeYear)) {
                    Term::create([
                        'label' => $value->denumire,
                        'start' => $value->inceput,
                        'end' => $value->sfarsit,
                        'college_year_id' => $collegeYear->id
                    ]);
                }
            }
        }

//        Groups
        $data = $this->loadSheet('Groups', $file);
        if(!empty($data) && $data->count()){
            foreach ($data as $key => $value) {
                $collegeYear = CollegeYear::where('label', $value->anul)->first();

                Group::create([
                    'label' => $value->nume,
                    'college_year_id' => $collegeYear->id
                ]);
            }
        }

//        Subgroups
        $data = $this->loadSheet('Subgroups', $file);
        if(!empty($data) && $data->count()){
            foreach ($data as $key => $value) {
                $group = Group::where('label', $value->grupa)->first();

                Subgroup::create([
                    'label' => $value->denumire,
                    'group_id' => $group->id
                ]);
            }
        }


//        Disciplines
        $data = $this->loadSheet('Disciplines', $file);
        if(!empty($data) && $data->count()){
            foreach ($data as $key => $value) {

                $term = Term::where('label', $value->semestru.' - '.$value->anul)->first();
//                dd($term);

                if (!is_null($term)) {
                    $discipline = Discipline::create([
                        'name' => $value->nume,
                        'number_hours_course' => $value->c,
                        'number_hours_seminary' => $value->s,
                        'number_hours_laboratory' => $value->l,
                        'number_hours_project' => $value->p,
                        'term_id' => $term->id
                    ]);

//                    Daca orele de curs sunt diferite de zero cream discipline_module pentru fiecare an in parte
                    if ($value->c != 0){
                        $disciplineModule = new DisciplineModule();
                        $disciplineModule->discipline_id = $discipline->id;
                        $disciplineModule->module_id = 1;
                        $disciplineModule->college_year_id = $term->collegeYear->id;
                        $disciplineModule->save();
                    }
//                    Cream pentru seminare
                    if ($value->s != 0){
                        foreach ($term->collegeYear->groups as $group){

                            $disciplineModule = new DisciplineModule();
                            $disciplineModule->discipline_id = $discipline->id;
                            $disciplineModule->module_id = 3;
                            $disciplineModule->group_id = $group->id;
                            $disciplineModule->save();
//                            dd($disciplineModule);
                        }
                    }
//                    Cream pentru proiecte
                    if ($value->p != 0){
                        foreach ($term->collegeYear->groups as $group){
                            $disciplineModule = new DisciplineModule();
                            $disciplineModule->discipline_id = $discipline->id;
                            $disciplineModule->module_id = 4;
                            $disciplineModule->group_id = $group->id;
                            $disciplineModule->save();
                        }
                    }
//                    Cream pentru laboratoare
                    if ($value->l != 0){
                        $groups = Group::where('college_year_id', $term->collegeYear->id)->get();
                        foreach ($groups as $group){
                            foreach ($group->subgroups as $subgroup){
                                $disciplineModule = new DisciplineModule();
                                $disciplineModule->discipline_id = $discipline->id;
                                $disciplineModule->module_id = 2;
                                $disciplineModule->subgroup_id = $subgroup->id;
                                $disciplineModule->save();
                            }
                        }
                    }
                }
            }
        }

//        Students
        $data = $this->loadSheet('Students', $file);
        if(!empty($data) && $data->count()){
            foreach ($data as $key => $value) {
                $subgroup = Subgroup::where('label', $value->subgrupa)->first();

                Student::create([
                    'first_name' => $value->prenume,
                    'last_name' => $value->nume,
                    'subgroup_id' => $subgroup->id
                ]);
            }
        }
//        Teachers
        $data = $this->loadSheet('Teachers', $file);
        if(!empty($data) && $data->count()){
            foreach ($data as $key => $value) {

                Teacher::create([
                    'first_name' => $value->prenume,
                    'last_name' => $value->nume,
                ]);
            }
        }

//        TODO: generare saptamani pe semestru
//        TODO: generare discipline_module
        $firstTerm = Term::first();
        $this->generateWeeks(1, $firstTerm->start, $firstTerm->end, $firstTerm);
        $secondTerm = Term::skip(1)->first();
        $this->generateWeeks(2, $secondTerm->start, $secondTerm->end, $secondTerm);

        return redirect()->back()->with('successes', ['Date incarcate!']);
    }

    protected  function generateWeeks($term, $term_start, $term_end, $termObject){

        $Date2 = $term_end;
        $week = new Week();

        for ($i=1 ; $i<=20; $i++) { // nr maxim de saptamani pe un semestru
            $Date1 = $term_start;
            $week->setLabel($i);
            $week->setStartWeek($Date1);
            $data = date('Y-m-d', strtotime($week->getStartWeek() . ' +7 day'));
            if ($data < $Date2) {
                $term_start = $data;
                if ($term == 1) {
                    if ($week->getLabel() == 13 || 	$week->getLabel() == 14 || $week->getLabel() == 20) {
                        $type_week = 'vacanta';
                    } else if ($week->getLabel() == 17 || $week->getLabel() == 18 || $week->getLabel() == 19) {
                        $type_week = 'sesiune';
                    } else {
                        $type_week = 'activitate';
                    }
                }
                if ($term == 2) {
                    if ($week->getLabel() == 8) {
                        $type_week = 'vacanta';
                    } else {
                        $type_week = 'activitate';
                    }
                }
                $week->setEndWeek($data);
                $week ->create([
                    "label" => 'Saptamana '.$week->getLabel(),
                    "start" => $week->getStartWeek(),
                    "end" => $week->getEndWeek(),
                    "week_type" => $type_week,
                    "term_id" => $termObject->id
                ]);
            }
        }
    }

    protected function weekIdPerCurrentWeek() {
        $week = new Week();
        $today = date(Y-m-d);

        if ($today >= $week->getStartWeek() && $today < $week->getEndWeek()) {
            $week_id = $week->getId();
        }
        return $week_id;
    }

    protected function loadSheet($sheet, $file){
        $data = Excel::selectSheets($sheet)->load($file, function($reader) {
        })->get();

        return $data;
    }

    public function design()
    {
        return view('dashboard.design');
    }
    public function emptyTables() {
        DB::statement("SET foreign_key_checks=0");
        DB::table('attendances')->truncate();
        DB::table('students')->truncate();
        DB::table('discipline_module_weeks')->truncate();
        DB::table('discipline_modules')->truncate();
        DB::table('statuses')->truncate();
        DB::table('weeks')->truncate();
        DB::table('modules')->truncate();
        DB::table('rotations')->truncate();
        DB::table('subgroups')->truncate();
        DB::table('groups')->truncate();
        DB::table('disciplines')->truncate();
        DB::table('terms')->truncate();
        DB::table('college_years')->truncate();
        DB::table('years')->truncate();
        DB::table('users')->truncate();
        DB::table('teachers')->truncate();
        DB::table('rights')->truncate();
        DB::statement("SET foreign_key_checks=1");
        Artisan::call('db:seed');
    }

    public function attendance(Request $request)
    {

        dd($request->all());
//codul asta de mai jos l-am mutat in AttendanceController
//        foreach ($request->students as $studentId){
////            Asa luati fiecare id de student bifat si lucrati cu el
//            dd($studentId);
//            $attendance = new Attendance();
//            $attendance->student_id = $studentId;
//            $attendance->discipline_module_week_id = $request-> input ('discipline_module_week_id');
//            $attendance ->saveAttendance($request);
//            return redirect()->route('dashboard.teacher')->with('successes', ['Prezenta studentilor a fost salvata!']);
//        }
    }
}
