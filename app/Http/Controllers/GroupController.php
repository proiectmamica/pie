<?php

namespace App\Http\Controllers;

use App\CollegeYear;
use App\Group;
use Illuminate\Http\Request;

class GroupController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        view()->share('adminMenu', 'groups');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('group.index')
            ->with('groups', Group::orderBy('created_at', 'desc')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $college_years = CollegeYear::all();
        return view('group.create')
            ->with('college_years', $college_years);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $group = new Group();
        $group->saveGroup($request);

        return redirect()->route( 'group.index')->with('successes', ['Grupa a fost salvata!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $college_years = CollegeYear::all();
        $group = Group::find($id);
        return view('group.edit')->with('group', $group)
                                        ->with('college_years', $college_years);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $group = Group::find($id);
        $group->updateGroup($request);

        return redirect()->route('group.index')->with('successes', ['Grupa s-a actualizat!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
