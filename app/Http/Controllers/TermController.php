<?php

namespace App\Http\Controllers;

use App\CollegeYear;
use App\Term;
use App\Year;
use Illuminate\Http\Request;

class TermController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        view()->share('adminMenu', 'terms');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('term.index')
            ->with('terms', Term::orderBy('created_at', 'desc')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $college_years = CollegeYear::all();
        return view('term.create')
            ->with('college_years', $college_years);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $term = new Term();
        $term->saveTerm($request);

        return redirect()->route('term.index')->with('successes', ['Semestruul a fost salvat!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $college_years =  CollegeYear::all();
        $term = Term::find($id);

        return view('term.edit')->with('term', $term)
                                      ->with('college_years',$college_years);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $term = Term::find($id);
        $term->updateTerm($request);

        return redirect()->route('term.index')->with('successes', ['Semestru actualizat!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
