<?php
/**
 * Created by PhpStorm.
 * User: lory
 * Date: 11/26/2017
 * Time: 4:30 PM
 */

namespace App\Http\Controllers;


use App\Attendance;
use App\DisciplineModule;
use App\DisciplineModuleWeek;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Week;
use Illuminate\Support\Facades\Auth;

class AttendanceController extends Controller
{

    public function __construct()
    {
        parent::__construct();
        view()->share('adminMenu', 'attendances');
    }/**
/**
 * Display a listing of the resource.
 *
 * @return \Illuminate\Http\Response
 */
    public function index()
    {
        return view('attendance.index')
            ->with('attendances', Attendance::orderBy('id', 'asc')->get());
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        foreach ($request->students as $studentId) {
            $attendance = new Attendance();
            $attendance->discipline_module_week_id = $request->input('discipline_module_week_id');
            if (hasAttendance($studentId,$attendance->discipline_module_week_id ) == false) {
                $attendance->student_id = $studentId;
                $attendance->save();
                $disciplineModulesWeek = DisciplineModuleWeek::find($request->input('discipline_module_week_id'));
                if ($disciplineModulesWeek->disciplineModule->teacher_id == Auth::user()->teacher_id)
                    $disciplineModulesWeek->status_id = 2;
                $disciplineModulesWeek->update();

            }
        }

        return redirect()->back()->with('successes', ['Prezenta studentilor a fost salvata!']);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }
}