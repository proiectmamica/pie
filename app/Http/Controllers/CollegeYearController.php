<?php

namespace App\Http\Controllers;

use App\CollegeYear;
use App\Year;
use Illuminate\Http\Request;

class CollegeYearController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        view()->share('adminMenu', 'college_years');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('college_year.index')
            ->with('college_years', CollegeYear::orderBy('created_at', 'desc')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $years = Year::all();
        return view('college_year.create')
            ->with('years', $years);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $college_year = new CollegeYear();
        $college_year->saveCollegeYear($request);

        return redirect()->route('college_year.index')->with('successes', ['Anul de facultate a fost creat!']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $years = Year::all();
        $college_year = CollegeYear::find($id);

        return view('college_year.edit')->with('college_year', $college_year)
                                              ->with('years', $years);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $college_year = CollegeYear::find($id);
        $college_year->updateCollegeYear($request);

        return redirect()->route('college_year.index')->with('successes', ['An actualizat!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
