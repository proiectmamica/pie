<?php

namespace App\Http\Controllers;

use App\Group;
use App\Subgroup;
use Illuminate\Http\Request;

class SubgroupController extends Controller
{
    public function __construct()
    {
        parent::__construct();
        view()->share('adminMenu', 'subgroups');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {
        return view('subgroup.index')
            ->with('subgroups', Subgroup::orderBy('created_at', 'desc')->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $groups = Group::all();
        return view('subgroup.create')
            ->with('groups', $groups);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $subgroup = new Subgroup();
        $subgroup ->saveSubgroup($request);

        return redirect()->route('subgroup.index')->with('successes', ['Subgrupa a fost salvata!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $groups = Group::all();
        $subgroup = Subgroup::find($id);

//        testing
        return view('subgroup.edit')->with('subgroup', $subgroup)
                                          ->with('groups',$groups);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subgroup = Subgroup::find($id);
        $subgroup->updateSubgroup($request);

        return redirect()->route('subgroup.index')->with('successes', ['Subgrupa actualizata!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
