<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DisciplineModule extends Model
{
    public function inputs($request)
    {
        $this->discipline_id = $request->input('discipline');
        $this->module_id = $request->input('module');
        $this->college_year_id = $request->input('college_year');
        $this->group_id = $request->input('group');
        $this->subgroup_id = $request->input('subgroup');
        $this->teacher_id = $request->input('teacher');
        $this->rotation_id = $request->input('rotation');

    }

    public function saveDisciplineModule($request)
    {
        $this->inputs($request);

        return $this->save();
    }

    public function updateDisciplineModule($request)
    {
        $this->inputs($request);

        return $this->update();
    }

    public function discipline()
    {
        return $this->belongsTo(Discipline::class);
    }

    public function module()
    {
        return $this->belongsTo(Module::class);
    }
    public function collegeYear()
    {
        return $this->belongsTo(CollegeYear::class, 'college_year_id');
    }
    public function group()
    {
        return $this->belongsTo(Group::class);
    }
    public function subgroup()
    {
        return $this->belongsTo(Subgroup::class);
    }
    public function teacher()
    {
        return $this->belongsTo(Teacher::class);
    }

    public function rotation()
    {
        return $this->belongsTo(Rotation::class);
    }

    public function attendances()
    {
        return $this->hasManyThrough(Attendance::class, DisciplineModuleWeek::class);
    }

    public function subgroupStudents()
    {
        return $this->hasMany(Student::class, 'subgroup_id', 'subgroup_id');
    }
}
