<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rotation extends Model
{
    public function discipline_modules() {
        return $this->hasMany(DisciplineModule::class);
    }
}
