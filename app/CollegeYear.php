<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollegeYear extends Model
{


    public function inputs($request)
    {
        $this->label = $request->input('label');
    }

    public function saveCollegeYear($request)
    {
        $this->inputs($request);

        return $this->save();
    }

    public function updateCollegeYear($request)
    {
        $this->inputs($request);

        return $this->update();
    }
    public function year() {
        return $this->belongsTo(Year::class);
    }

    public function groups()
    {
        return $this->hasMany(Group::class);
    }
    public function terms()
    {
        return $this->hasMany(Term::class);
    }
    public function discipline_modules() {
        return $this->hasMany(DisciplineModule::class);
    }
}
