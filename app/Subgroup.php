<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subgroup extends Model
{
    protected $fillable = ['label', 'group_id'];

    public function inputs($request)
    {
        $this->label = $request->input('label');
        $this->group_id = $request->input('group');
    }

    public function saveSubgroup($request) {
        $this->inputs($request);

        return $this->save();
    }

    public function updateSubgroup($request) {
        $this->inputs($request);

        return $this->update();
    }

    public function group()
    {
        return $this->belongsTo(Group::class);
    }
    public function discipline_modules() {
        return $this->hasMany(DisciplineModule::class);
    }

    public function students() {
        return $this->hasMany(Student::class);
    }
}
