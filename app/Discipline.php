<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discipline extends Model
{
    protected $fillable = ['name', 'number_hours_course', 'number_hours_seminary', 'number_hours_laboratory', 'number_hours_project','term_id'];

    public function inputs($request)
    {
        $this->name = $request->input('name');
        $this->number_hours_course = $request->input('number_hours_course');
        $this->number_hours_seminary = $request->input('number_hours_seminary');
        $this->number_hours_laboratory = $request->input('number_hours_laboratory');
        $this->number_hours_project = $request->input('number_hours_project');
        $this->term_id = $request->input('term');
    }

    public function saveDiscipline($request)
    {
        $this->inputs($request);

        return $this->save();
    }

    public function updateDiscipline($request)
    {
        $this->inputs($request);

        return $this->update();
    }

    public function term() {
        return $this->belongsTo(Term::class);
    }
    public function discipline_modules() {
        return $this->hasMany(DisciplineModule::class);
    }
}
