<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    public function inputs($request) {
        $this->label = $request->input('label');
    }

    public function saveModule($request) {
        $this->inputs($request);

        return $this->save();
    }

    public function updateModule($request) {
        $this->inputs($request);

        return $this->update();
    }
    public function discipline_modules() {
        return $this->hasMany(DisciplineModule::class);
    }

}
