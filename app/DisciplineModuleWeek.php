<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class DisciplineModuleWeek extends Model
{
    public function saveDisciplineModuleWeek($discipline, $week, $status)
    {
        $this->discipline_module_id = $discipline;
        $this->week_id = $week;
        $this->status_id = $status;

        return $this->save();
    }

    public function disciplineModule()
    {
        return $this->belongsTo(DisciplineModule::class,'discipline_module_id');
    }

    public function attendances() {
        return $this->hasMany(Attendance::class);
    }
    public function status()  {
        return $this->belongsTo(Status::class);
    }
    public function week() {
        return $this->belongsTo(Week::class);
    }
}
