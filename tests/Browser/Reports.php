<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class Reports extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     * @throws \Throwable
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser
                ->loginAs(User::find(14))
                ->visit('/report')
                ->keys('input[type=search]', 'PCLP')
//                ->click('#report-discipline-24')
//                ->waitFor('#report-total-24')
//                ->click('#report-total-24')
                ->pause(3000)
                ;
        });
    }
}
