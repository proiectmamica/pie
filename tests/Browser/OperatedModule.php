<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class OperatedModule extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     * @throws \Throwable
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser
                ->loginAs(User::find(14))
                ->visit('/discipline-module/operated')
                ->click('#course-link-13')
                ->waitFor('#course-13-student-1')
//                ->pause(1000)
                ->uncheck('#course-13-student-1')
                ->click('button[type=submit]')
                ->pause(3000)
            ;
        });
    }
}
