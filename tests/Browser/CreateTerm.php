<?php

namespace Tests\Browser;

use App\User;
use Tests\Browser\Pages\Term;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CreateTerm extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     * @throws \Throwable
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser
                ->loginAs(User::find(1))
                ->visit('/term/create')
                ->on(new Term())
                ->createTerm('TEST', 3)
                ->pause(2000)
                ;
        });
    }
}
