<?php

namespace Tests\Browser;

use App\User;
use Tests\Browser\Pages\Assign;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class AssignMultipleModules extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     * @throws \Throwable
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser
                ->loginAs(User::find(1))
                ->visit('/')
                ->clickLink('Atribuie profesori')
                ->on(new Assign())
                ->assignMultipleModules(33)
                ->pause(2000)
            ;
        });
    }
}
