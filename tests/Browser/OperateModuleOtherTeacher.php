<?php

namespace Tests\Browser;

use App\User;
use Tests\Browser\Pages\Module;
use Tests\Browser\Pages\Teacher;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class OperateModuleOtherTeacher extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     * @throws \Throwable
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser
                ->loginAs(User::find(14))
                ->visit('/')
                ->on(new Teacher())
                ->chooseAnotherTeacher(12,32)
                ->on(new Module())
                ->operateModule('#laboratory-link-105', '#laboratory-105-student-211')
                ->pause(3000)

            ;
        });
    }
}
