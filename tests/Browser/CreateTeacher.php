<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CreateTeacher extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser
                ->loginAs(User::find(1))
                ->visit('/teacher/create')
                ->type('first_name', 'TEST')
                ->type('last_name', 'TEST')
                ->click('button[type=submit]')
                ->pause(2000)
            ;
        });
    }
}
