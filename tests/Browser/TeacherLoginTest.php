<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class TeacherLoginTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     * @throws \Throwable
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                ->assertSee('Sign in')
                ->type('email', 'diana.stefanescu@ugal.ro')
                ->type('password', 'dmSfPQ' )
                ->click('button[type=submit]')
                ->assertPathIs('/')
            ;
        });
    }
}
