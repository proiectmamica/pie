<?php

namespace Tests\Browser;

use App\User;
use Tests\Browser\Pages\Teacher;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ChooseOtherTeacher extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     * @throws \Throwable
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser
                ->loginAs(User::find(14))
                ->visit('/')
                ->on(new Teacher())
                ->chooseAnotherTeacher(12,31)
                ->wait(3000)
            ;
        });
    }
}
