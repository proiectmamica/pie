<?php

namespace Tests\Browser;

use App\User;
use Tests\Browser\Pages\Module;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class OperateModule extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     * @throws \Throwable
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser
                ->loginAs(User::find(14))
                ->visit('/')
                ->on(new Module())
                ->operateModule('#laboratory-link-39', '#check-all-39')
                ->pause(2000)
            ;
        });
    }
}
