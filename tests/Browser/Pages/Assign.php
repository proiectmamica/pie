<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

class Assign extends BasePage
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/discipline_module/assign';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@discipline' => 'discipline_modules[]',
            '@teacher' => 'teacher',
            '@rotation' => 'rotation-10',
            '@button' => 'button[type=submit]',
            '@confirm' => '.swal2-confirm'
        ];
    }

    public function assignToTeacher(Browser $browser, $module, $teacher)
    {
        $browser
            ->check('@discipline', $module)
            ->radio('@teacher', $teacher)
            ->click('@button')
            ->pause(2000)
            ->click('@confirm')
            ;
    }

    public function assignMultipleModules(Browser $browser, $teacher)
    {
        $browser
            ->check('@discipline', 9)
            ->check('@discipline', 10)
            ->radio('@rotation', 2 )
            ->radio('@teacher', $teacher)
            ->click('@button')
            ->pause(2000)
            ->click('@confirm')
            ;
    }
}
