<?php

namespace Tests\Browser\Pages;

use App\User;
use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

class Term extends BasePage
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/term/create';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@label' => 'input[name=label]',
            '@year' => 'college_year',
            '@start' => 'input[name=start]',
            '@end' => 'input[name=end]',
            '@button' => 'button[type=submit]'
        ];
    }

    public function createTerm(Browser $browser, $label, $year)
    {
        $browser
            ->type('@label', $label)
            ->select('@year', $year)
            ->keys('@start', '122',['{tab}'], '2018')
            ->keys('@end', '127',['{tab}'], '2018')
            ->click('@button')
            ;
    }
}
