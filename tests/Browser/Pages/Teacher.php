<?php

namespace Tests\Browser\Pages;

use Laravel\Dusk\Browser;
use Laravel\Dusk\Page as BasePage;

class Teacher extends BasePage
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/';
    }

    /**
     * Assert that the browser is on the page.
     *
     * @param  Browser  $browser
     * @return void
     */
    public function assert(Browser $browser)
    {
        $browser->assertPathIs($this->url());
    }

    /**
     * Get the element shortcuts for the page.
     *
     * @return array
     */
    public function elements()
    {
        return [
            '@teacher' => 'teacher',
            '@week' => 'week',
            '@submit' => '#change-teacher',
            '@button' => '#another-teacher'
        ];
    }

    public function chooseAnotherTeacher(Browser $browser, $teacher, $week)
    {
        $browser
            ->click('@button')
            ->waitFor('#teachers')
            ->select('@teacher', $teacher)
            ->select('@week', $week)
            ->click('@submit')
            ;
    }


}
