@extends('layouts.admin')

@section('title')
    Adauga Disciplina
@stop


@section('content')
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{route('discipline.index')}}">Discipline</a></li>
            <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
        </ol>
    </nav>
    <h2 class="text-center mt-30">Adauga disciplina</h2>

    <form action="{{route('discipline.store')}}" method="post">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4">

                {{ csrf_field() }}
                <label for="label" class="control-label">Denumire</label>
                <input type="text" name="name" id="name">
                <label for="label" class="control-label">Numar ore curs</label>
                <input type="text" name="number_hours_course" id="number_hours_course">
                <label for="label" class="control-label">Numar ore seminar</label>
                <input type="text" name="number_hours_seminary" id="number_hours_seminary">
                <label for="label" class="control-label">Numar ore laborator</label>
                <input type="text" name="number_hours_laboratory" id="number_hours_laboratory">
                <label for="label" class="control-label">Numar ore proiect</label>
                <input type="text" name="number_hours_project" id="number_hours_project">
                <label for="terms" class="control-label">Alegeti semestru</label>
                <select name="term" id="term" class="form-control">
                    @foreach($terms as $term)
                        <option value="{{$term->id}}">{{$term->label}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-sm-4 col-sm-offset-4">
                <button type="submit" class="btn green pull-right mt-15">Salveaza</button>
            </div>
        </div>
    </form>

@stop
