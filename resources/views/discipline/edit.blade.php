@extends('layouts.admin')

@section('title')
    Editeaza Disciplina {{$discipline->name}}
@stop


@section('content')
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{route('discipline.index')}}">Discipline</a></li>
            <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
        </ol>
    </nav>
    <h2 class="mt-30 text-center">Editeaza Disciplina {{$discipline->name}}</h2>

    <form action="{{route('discipline.update', ['id' => $discipline->id])}}" method="post">
        <div class="row mt-30">
            <div class="col-sm-4 col-sm-offset-4">

                {{ csrf_field() }}
                <label for="label" class="control-label">Denumire</label>
                <input type="text" name="name" id="name" value="{{$discipline->name}}">
                <div class="mt-30"></div>
                <label for="label" class="control-label">Numar ore curs</label>
                <input type="text" name="number_hours_course" id="number_hours_course" value="{{$discipline->number_hours_course}}">
                <div class="mt-30"></div>
                <label for="label" class="control-label">Numar ore seminar</label>
                <input type="text" name="number_hours_seminary" id="number_hours_seminary" value="{{$discipline->number_hours_seminary}}">
                <div class="mt-30"></div>
                <label for="label" class="control-label">Numar ore laborator</label>
                <input type="text" name="number_hours_laboratory" id="number_hours_laboratory" value="{{$discipline->number_hours_laboratory}}">
                <div class="mt-30"></div>
                <label for="label" class="control-label">Numar ore proiect</label>
                <input type="text" name="number_hours_project" id="number_hours_project" value="{{$discipline->number_hours_project}}">
                <div class="mt-30"></div>
                <label for="label" class="control-label">Alegesti semestrul</label>
                <select name="group" id="group" class="form-control">
                    @foreach($terms as $term)
                        <option value="{{$term->id}}" @if($term->id == $discipline->term_id) selected @endif> {{$term->label}}</option>
                    @endforeach
                </select>

            </div>
            <div class="col-sm-4 col-sm-offset-4">
                <button type="submit" class="btn green pull-right mt-15">Salveaza</button>
            </div>
        </div>
    </form>

@stop
