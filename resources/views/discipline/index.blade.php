@extends('layouts.admin')

@section('title')
    Discipline
@stop

@section('styles')
    <link href="{{asset('css/jquery.dataTables.min.css')}}" rel="stylesheet">
@stop


@section('content')
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-sm-6">
            <h1>Discipline</h1>
        </div>
        <div class="col-md-6 mt-20">
            <a href="{{route('discipline.create')}}" class="btn green pull-right mb-10"><i class="fa fa-plus"></i> Adauga</a>
        </div>
    </div>
    <table class="table mt-30" id="example">
        <thead>
        <tr>
            <th >Semestru</th>
            <th>Nume</th>
            <th>Curs</th>
            <th>Seminar</th>
            <th>Laborator</th>
            <th>Proiect</th>
            <th>Actiuni</th>
            <th>Detalii</th>
        </tr>
        </thead>
        <tbody>
        @foreach($disciplines as $discipline)
            <tr>
                <td>{{$discipline->term->label}}</td>
                <td>{{$discipline->name}}</td>
                <td>{{$discipline->number_hours_course}}</td>
                <td>{{$discipline->number_hours_seminary}}</td>
                <td>{{$discipline->number_hours_laboratory}}</td>
                <td>{{$discipline->number_hours_project}}</td>
                <td>
                    <a href="{{route('discipline.edit', ['id' => $discipline->id])}}" class="btn blue"><i class="fa fa-pencil-square"></i> Editeaza</a>
                </td>
                <td>
                    Creat la: {{$discipline->created_at}} <br>
                    Actualizat la : {{$discipline->updated_at}}
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>


@stop

@section('scripts')
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
@stop