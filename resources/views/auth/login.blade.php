<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Gestiune absente | Login</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('css/custom.css')}}" rel="stylesheet">
    <link href="{{asset('css/toastr.css')}}" rel="stylesheet">

</head>
<body>
<div id="loginpage">
    <img id="loginbg" src="{{asset('images/udj.jpg')}}"/>
    <div class="bg-overlay">
        <div id="login-box">
            <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                <h3 class="title-app">Gestiune prezenta studenti</h3>
                <h3 class="login-title">Login</h3>
                <input type="text" placeholder="Email" name="email" />
                <input type="password" placeholder="Parola" name="password"/>
                <button type="submit" class="btn green" id="sign-in">Sign in</button>
            </form>
            <div id="login-right">
                <p>Universitatea<br/>"Dunarea de Jos", Galati</p>
                <img src="http://www.teme.ugal.ro//img/logo-ugal-large.png"/>
            </div>
        </div>

    </div>
</div>

<script src="{{asset('js/jquery-3.2.1.js')}}"></script>
<script src="{{asset('js/bootstrap.js')}}"></script>
<script src="{{asset('js/custom.js')}}"></script>
<script src="{{asset('js/toastr.js')}}"></script>
<script>

    {{--@if(!is_array(session('errors')) && get_class(session('errors')) == 'Illuminate\Support\ViewErrorBag')--}}
            {{--{{changeSessionToNormal()}}--}}
            {{--@endif--}}
            {{--@if(session('errors'))--}}
            {{--@foreach(session('errors') as $notification)--}}
        {{--toastr["error"]("{{$notification}}", 'Eroare!');--}}
    {{--@endforeach--}}
            {{--@endif--}}
            {{--@if(session('warnings'))--}}
            {{--@foreach(session('warnings') as $notification)--}}
        {{--toastr["warning"]("{{$notification}}", "Atentie!");--}}
    {{--@endforeach--}}
            {{--@endif--}}
            {{--@if(session('successes'))--}}
            {{--@foreach(session('successes') as $notification)--}}
        {{--toastr["success"]("{{$notification}}", "Succes!");--}}
    {{--@endforeach--}}
            {{--@endif--}}
            {{--@if(session('infos'))--}}
            {{--@foreach(session('infos') as $notification)--}}
        {{--toastr["info"]("{{$notification}}", "Info!");--}}
    {{--@endforeach--}}
            {{--@endif--}}

            {{--@if(session('notifies'))--}}
            {{--@foreach(session('notifies') as $notification)--}}
        {{--toastr["info"]("{{$notification}}", "Notificare!");--}}
    {{--@endforeach--}}
            {{--@endif--}}

            {{--@if(session('error_fields'))--}}
            {{--@foreach(session('error_fields') as $notification)--}}
        {{--toastr["error"]("Completati campul \"{{ucfirst($notification)}}\"", "Eroare!");--}}
    {{--@endforeach--}}
    {{--@endif--}}
</script>
</body>
</html>