@extends('layouts.admin')

@section('title')
    Adauga saptamana
@stop


@section('content')
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{route('week.index')}}">Saptamani</a></li>
            <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
        </ol>
    </nav>
    <h2 class="text-center mt-30">Adauga saptamana</h2>

    <form action="{{route('week.store')}}" method="post">
        <div class="row mt-30">
            <div class="col-md-4 col-sm-offset-4">

                {{ csrf_field() }}
                <label for="label" class="control-label">Saptamana</label>
                <input type="text" name="label" id="label" required>
                <div class="mt-30"></div>
                <label for="week_type" class="control-label">Activitate universitara</label>
                <input type="text" name="week_type" id="week_type" required>
                <div class="mt-30"></div>
                <label for="term" class="control-label">Semestrul</label>
                <select name="term" class="form-control" id="">
                    @foreach($terms as $term)
                        <option value="{{$term->id}}" @if(isset($week) && $term->id == $week->id) selected @endif>{{$term->label}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-4 col-sm-offset-4 mt-30">
                <label for="start" class="control-label">Data de incepere</label>
                <input type="date" id="start" name="start" class="datepicker">
                <div class="mt-30"></div>
                <label for="start" class="control-label">Data de sfarsit</label>
                <input type="date" id="start" name="end" class="datepicker">

            </div>
            <div class="col-md-4 col-sm-offset-4">
                <button type="submit" class="btn green pull-right mt-15">Salveaza</button>
            </div>
        </div>
    </form>

@stop