@extends('layouts.admin')

@section('title')
    Saptamani
@stop

@section('content')

    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-sm-6">
            <h1>Saptamani</h1>
        </div>
        <div class="col-md-6 mt-20">
            <a href="{{route('week.create')}}" class="btn green pull-right mb-10"><i class="fa fa-plus"></i> Adauga</a>
        </div>
    </div>

    <table class="table mt-30">
        <thead>
        <tr>
            <th>Label</th>
            <th>Durata</th>
            <th>Activitate universitara</th>
            <th>Semestru</th>
            <th>Actiuni</th>
            <th>Detalii</th>
        </tr>
        </thead>
        <tbody>
        @foreach($weeks as $week)
            <tr>
                <td>{{$week->label}}</td>
                <td>{{$week->start}} - {{$week->end}}</td>
                <td>
                    {{$week->week_type}}
                </td>
                <td>
                    {{$week->term->label}}
                </td>
                <td>
                    <a href="{{route('week.edit', ['id' => $week->id])}}" class="btn blue"><i class="fa fa-pencil-square"></i> Editeaza</a>
                </td>
                <td>
                    Creat la: {{$week->created_at}} <br>
                    Actualizat la : {{$week->updated_at}}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


@stop