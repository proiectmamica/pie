<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Gestiune absente</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .column-width {
            width: 5%;
            text-align: center;
        }
    </style>

</head>
<body>


<table>
    <tr>
        <td style="width: 15%">
            <img style="height: 100px" src="{{asset('images/logo-ugal-large.png')}}">
        </td>
        <td style="width: 45%;vertical-align:middle; color: #4982b7">
            <h2 style="margin: 0 0 0 20px;font-size: 16px;">Universitatea Dunarea de Jos Galati</h2>
            <p style="margin-top: 3px; margin-left: 20px;">Facultatea de Automatica, Calculatoare, Inginerie Electrica si Electronica</p>
        </td>
        <td style="width: 40%">

        </td>
    </tr>
</table>

<h1 style="text-align: center;margin-bottom: 0;">{{ $student->last_name}} {{$student->first_name}}</h1>
<h2 style="text-align:center;margin-top: 0;margin-bottom: 10px;">Prezenta pe discipline</h2>




<table style="width: 100%; margin-top: 20px; border-collapse: collapse; vertical-align: middle;" border="1">
    <tr>
        <td style="width: 30%; padding-left: 15px;">Disciplina</td>
        <td class="column-width">Prezenta Curs</td>
        <td class="column-width">Prezenta Seminar</td>
        <td class="column-width">Prezenta Laborator</td>
        <td class="column-width">Prezenta Proiect</td>

    </tr>

    @foreach($disciplines as $discipline)
        <tr>
            <td style="padding-left: 15px;">{{$discipline->name}}</td>
            <td class="column-width">
                @if($discipline->number_hours_course != 0 && isset($course[$discipline->id]))
                    {{ $course[$discipline->id]['attendances'] }} / {{ $course[$discipline->id]['total'] }}
                @else
                    -
                @endif
            </td>
            <td class="column-width">
                @if($discipline->number_hours_seminary != 0 && isset($seminary[$discipline->id]))
                    {{ $seminary[$discipline->id]['attendances'] }} / {{ $seminary[$discipline->id]['total'] }}
                @else
                    -
                @endif
            </td>
            <td class="column-width">
                @if($discipline->number_hours_laboratory != 0 && isset($laboratory[$discipline->id]))
                    {{ $laboratory[$discipline->id]['attendances'] }} / {{ $laboratory[$discipline->id]['total'] }}
                @else
                    -
                @endif
            </td>
            <td class="column-width">
                @if($discipline->number_hours_project != 0 && isset($project[$discipline->id]))
                    {{ $project[$discipline->id]['attendances'] }} / {{ $project[$discipline->id]['total'] }}
                @else
                    -
                @endif
            </td>

        </tr>
    @endforeach
</table>

</body>
</html>

