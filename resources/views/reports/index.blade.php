@extends('layouts.admin')

@section('title')
    Rapoarte
@stop

@section('styles')
    <link href="{{asset('css/jquery.dataTables.min.css')}}" rel="stylesheet">
@stop

@section('content')

    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
        </ol>
    </nav>

    <ul class="nav nav-tabs reports-nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home">Discipline</a></li>
        <li><a data-toggle="tab" href="#menu1">Studenti</a></li>
    </ul>

    <div class="tab-content">
        <div id="home" class="tab-pane fade in active">
            <table class="table mt-30" id="disciplines">
                <thead>
                <tr>
                    <th>Disciplina</th>
                    <th>Rapoarte</th>
                </tr>
                </thead>
                <tbody>
                @foreach($disciplines as $discipline)
                    <tr>
                        <td>{{$discipline->name}}</td>
                        <td>
                            <div class="dropdown">
                                <button id="report-discipline-{{$discipline->id}}" class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Tipul Raportului
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a id="report-total-{{$discipline->id}}" href="{{route('report.total', ['id' => $discipline->id])}}">Numarul total de prezente</a></li>
                                    <li><a href="{{route('report.groups', ['id' => $discipline->id])}}">Prezentele pe Grupe</a></li>
                                    <li><a href="{{route('report.students', ['id' => $discipline->id])}}">Prezentele pe Studenti</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div id="menu1" class="tab-pane fade">
            <table class="table mt-30" id="example">
                <thead>
                <tr>
                    <th>Student</th>
                    <th>Rapoarte</th>
                </tr>
                </thead>
                <tbody>
                @foreach($students as $student)
                    <tr>
                        <td>{{$student->last_name}} {{$student->first_name}}</td>
                        <td>
                            <div class="dropdown">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Tipul Raportului
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a  href="{{route('report.student', ['id' => $student->id])}}">Prezenta pe discipline</a></li>
                                    <li><a href="{{route('report.weeks', ['id' => $student->id])}}">Prezentele pe Saptamana</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>

@stop

@section('scripts')
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#disciplines').DataTable();
            $('#example').DataTable();
        } );
    </script>
@stop