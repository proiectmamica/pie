<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Gestiune absente</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        .column-width {
            width: 5%;
            text-align: center;
        }
    </style>

</head>
<body>


<table>
    <tr>
        <td style="width: 15%">
            <img style="height: 100px" src="{{asset('images/logo-ugal-large.png')}}">
        </td>
        <td style="width: 45%;vertical-align:middle; color: #4982b7">
            <h2 style="margin: 0 0 0 20px;font-size: 16px;">Universitatea Dunarea de Jos Galati</h2>
            <p style="margin-top: 3px; margin-left: 20px;">Facultatea de Automatica, Calculatoare, Inginerie Electrica si Electronica</p>
        </td>
        <td style="width: 40%">

        </td>
    </tr>
</table>

<h1 style="text-align: center;margin-bottom: 0;">{{ $student->last_name}} {{$student->first_name}}</h1>
<h2 style="text-align:center;margin-top: 0;margin-bottom: 10px;">Prezenta pe saptamani</h2>




<table style="width: 100%; margin-top: 20px; border-collapse: collapse; vertical-align: middle;" border="1">
    <tr>
        <td class="column-width">Disciplina</td>
        <td class="column-width">Tip</td>
        @foreach($weeks as $week)
            <td class="column-width">{{$loop->index + 1}}</td>
        @endforeach
    </tr>
    @foreach($disciplines as $discipline)
        <tr>
            <td rowspan="4" class="column-width">{{$discipline->name}}</td>
            <td class="column-width">C</td>
            @foreach($weeks as $week)
                <td class="column-width">
                    @if($discipline->number_hours_course > 0 && isset($course[$discipline->id])  && isset($laboratory[$discipline->id][$week->id]))
                        {{$course[$discipline->id][$week->id]['attendances']}}
                    @else
                        -
                    @endif
                </td>
            @endforeach
        </tr>
        <tr>
            <td class="column-width">S</td>
            @foreach($weeks as $week)
                <td class="column-width" >
                    @if($discipline->number_hours_seminary > 0 && isset($seminary[$discipline->id])  && isset($seminary[$discipline->id][$week->id]))
                        {{$seminary[$discipline->id][$week->id]['attendances']}}
                    @else
                        -
                    @endif
                </td>
            @endforeach
        </tr>
        <tr>
            <td class="column-width">L</td>
            @foreach($weeks as $week)
                <td class="column-width" >
                    @if($discipline->number_hours_laboratory > 0 && isset($laboratory[$discipline->id]) && isset($laboratory[$discipline->id][$week->id]))
                        {{$laboratory[$discipline->id][$week->id]['attendances']}}
                    @else
                        -
                    @endif
                </td>
            @endforeach
        </tr>
        <tr>
            <td class="column-width">P</td>
            @foreach($weeks as $week)
                <td class="column-width" >
                    @if($discipline->number_hours_project > 0 && isset($project[$discipline->id])  && isset($project[$discipline->id][$week->id]))
                        {{$project[$discipline->id][$week->id]['attendances']}}
                    @else
                        -
                    @endif
                </td>
            @endforeach
        </tr>
    @endforeach
    {{--<tr>--}}
    {{--<td style="padding-left: 15px;">Module Operate</td>--}}
    {{--<td class="column-width">--}}
    {{--@if($discipline->number_hours_course > 0)--}}
    {{--{{getOperated($discipline, 1)}} / {{getNumberOfWeeks($discipline->number_hours_course)}}--}}
    {{--@else--}}
    {{-----}}
    {{--@endif--}}
    {{--</td>--}}
    {{--<td class="column-width">--}}
    {{--@if($discipline->number_hours_seminary > 0)--}}
    {{--{{getOperated($discipline, 3)}} / {{getNumberOfWeeks($discipline->number_hours_seminary)}}--}}
    {{--@else--}}
    {{-----}}
    {{--@endif--}}
    {{--</td>--}}
    {{--<td class="column-width">--}}
    {{--@if($discipline->number_hours_laboratory > 0)--}}
    {{--{{getOperated($discipline, 2)}} / {{getNumberOfWeeks($discipline->number_hours_laboratory)}}--}}
    {{--@else--}}
    {{-----}}
    {{--@endif--}}
    {{--</td>--}}
    {{--<td class="column-width">--}}
    {{--@if($discipline->number_hours_project > 0)--}}
    {{--{{getOperated($discipline, 4)}} / {{getNumberOfWeeks($discipline->number_hours_project)}}--}}
    {{--@else--}}
    {{-----}}
    {{--@endif--}}
    {{--</td>--}}
    {{--</tr>--}}
    {{--@foreach($disciplines as $discipline)--}}
    {{--<tr>--}}
    {{--<td style="padding-left: 15px;">{{$discipline->name}}</td>--}}
    {{--<td class="column-width">--}}
    {{--@if($discipline->number_hours_course != 0 && isset($course[$discipline->id]))--}}
    {{--{{ $course[$discipline->id]['attendances'] }} / {{ $course[$discipline->id]['total'] }}--}}
    {{--@else--}}
    {{-----}}
    {{--@endif--}}
    {{--</td>--}}
    {{--<td class="column-width">--}}
    {{--@if($discipline->number_hours_seminary != 0 && isset($seminary[$discipline->id]))--}}
    {{--{{ $seminary[$discipline->id]['attendances'] }} / {{ $seminary[$discipline->id]['total'] }}--}}
    {{--@else--}}
    {{-----}}
    {{--@endif--}}
    {{--</td>--}}
    {{--<td class="column-width">--}}
    {{--@if($discipline->number_hours_laboratory != 0 && isset($laboratory[$discipline->id]))--}}
    {{--{{ $laboratory[$discipline->id]['attendances'] }} / {{ $laboratory[$discipline->id]['total'] }}--}}
    {{--@else--}}
    {{-----}}
    {{--@endif--}}
    {{--</td>--}}
    {{--<td class="column-width">--}}
    {{--@if($discipline->number_hours_project != 0 && isset($project[$discipline->id]))--}}
    {{--{{ $project[$discipline->id]['attendances'] }} / {{ $project[$discipline->id]['total'] }}--}}
    {{--@else--}}
    {{-----}}
    {{--@endif--}}
    {{--</td>--}}

    {{--</tr>--}}
    {{--@endforeach--}}
</table>

</body>
</html>

