@extends('layouts.admin')

@section('title')
    Adauga Profesor
@stop


@section('content')
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{route('teacher.index')}}">Profesori</a></li>
            <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
        </ol>
    </nav>
    <h2 class="text-center mt-30">Adauga Profesor</h2>

    <form action="{{route('teacher.store')}}" method="post">
        <div class="row mt-30">
            <div class="col-md-4 col-md-offset-4">

                {{ csrf_field() }}
                <label for="label" class="control-label">Nume</label>
                <input type="text" name="first_name" id="first_name">
                <div class="mt-30"></div>

                <label for="label" class="control-label">Prenume</label>
                <input type="text" name="last_name" id="last_name">

            </div>
            <div class="col-md-4 col-md-offset-4 text-center">
                <button type="submit" class="btn green pull-right mt-15">Salveaza</button>
            </div>
        </div>
    </form>

@stop
