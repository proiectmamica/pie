<div id="side-menu">
    <ul>
        <li><a href="{{route('dashboard')}}"><i class="fa fa-home" aria-hidden="true"></i> <div class="link-name">Dashboard</div></a></li>
        @if(Auth::user()->right_id == 1)
            <li><a href="{{route('teacher.index')}}"><i class="fa fa-briefcase" aria-hidden="true"></i> <div class="link-name">Profesori</div></a></li>
            <li><a href="{{route('year.index')}}"><i class="fa fa-calendar" aria-hidden="true"></i> <div class="link-name">Ani</div></a></li>
            <li><a href="{{route('term.index')}}"><i class="fa fa-calendar-minus-o" aria-hidden="true"></i> <div class="link-name">Semestre</div></a></li>
            <li><a href="{{route('week.index')}}"><i class="fa fa-calendar-o" aria-hidden="true"></i> <div class="link-name">Saptamani</div></a></li>
            <li><a href="{{route('discipline.index')}}"><i class="fa fa-book" aria-hidden="true"></i> <div class="link-name">Disclipline</div></a></li>
            <li><a href="{{route('college_year.index')}}"><i class="fa fa-calendar-plus-o" aria-hidden="true"></i> <div class="link-name">An facultate</div></a></li>
            <li><a href="{{route('group.index')}}"><i class="fa fa-users" aria-hidden="true"></i> <div class="link-name">Grupe</div></a></li>
            <li><a href="{{route('subgroup.index')}}"><i class="fa fa-user-circle" aria-hidden="true"></i> <div class="link-name">Subgrupe</div></a></li>
            <li><a href="{{route('student.index')}}"><i class="fa fa-user" aria-hidden="true"></i> <div class="link-name">Studenti</div></a></li>
            <li><a href="{{route('discipline_module.index')}}"><i class="fa fa-calendar" aria-hidden="true"></i> <div class="link-name">Module discipline</div></a></li>
        @endif
        @if(Auth::user()->right_id == 2)
            <li><a href="{{route('discipline_module.operated')}}"><i class="fa fa-calendar" aria-hidden="true"></i> <div class="link-name">Module Operate</div></a></li>
            <li><a href="{{route('report.index')}}"><i class="fa fa-book" aria-hidden="true"></i> <div class="link-name">Rapoarte</div></a></li>
        @endif
    </ul>
</div>
<div id="side-menu-details">
    <ul>
        <li><a href="{{route('dashboard')}}">Dashboard</a></li>
        @if(Auth::user()->right_id == 1)
            <li><a href="{{route('teacher.index')}}">Profesori</a></li>
            <li><a href="{{route('year.index')}}">Ani</a></li>
            <li><a href="{{route('term.index')}}">Semestre</a></li>
            <li><a href="{{route('week.index')}}">Saptamani</a></li>
            <li><a href="{{route('discipline.index')}}">Discipline</a></li>
            <li><a href="{{route('college_year.index')}}">An Facultate</a></li>
            <li><a href="{{route('group.index')}}">Grupe</a></li>
            <li><a href="{{route('subgroup.index')}}">Subgrupe</a></li>
            <li><a href="{{route('student.index')}}">Studenti</a></li>
            <li><a href="{{route('discipline_module.index')}}">Module Discipline</a></li>
        @endif
        @if(Auth::user()->right_id == 2)
            <li><a href="{{route('discipline_module.operated')}}">Module Operate</a></li>
            <li><a href="{{route('report.index')}}">Rapoarte</a></li>
        @endif

    </ul>
</div>
