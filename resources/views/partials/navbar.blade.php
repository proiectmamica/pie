<div id="navbar">
    <div id="menu-btn">
        <i class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="pull-right" id="menu-buttons-right">
        <div id="username">{{Auth::user()->username}}</div>

        {{--<a href=""><i class="fa fa-cogs" aria-hidden="true"></i></a>--}}
        <a href="{{ route('logout') }}"
           onclick="event.preventDefault();document.getElementById('logout-form').submit();">
            <i class="fa fa-power-off" aria-hidden="true"></i>
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </div>
</div>