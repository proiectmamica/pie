@extends('layouts.admin')

@section('title')
    Adauga student
@stop


@section('content')
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{route('student.index')}}">Studenti</a></li>
            <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
        </ol>
    </nav>
    <h2 class="mt-30 text-center">Adauga student</h2>

    <form action="{{route('student.store')}}" method="post">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4">

                {{ csrf_field() }}
                <label for="label" class="control-label">Nume</label>
                <input type="text" name="first_name" id="first_name" required>
                <label for="label" class="control-label mt-30">Prenume</label>
                <input type="text" name="last_name" id="last_name" required>
                <label for="label" class="control-label mt-30">Alegeti subgrupa</label>
                <select name="subgroup" id="subgroup" class="form-control">
                    @foreach($subgroups as $subgroup)
                        <option value="{{$subgroup->id}}">{{$subgroup->label}}</option>
                    @endforeach
                </select>
                <button type="submit" class="btn green pull-right mt-15">Salveaza</button>
            </div>
        </div>

    </form>

@stop