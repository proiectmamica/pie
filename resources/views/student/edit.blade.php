@extends('layouts.admin')

@section('title')
    Editeaza studentul {{$student->first_name}} {{$student->last_name}}
@stop


@section('content')
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{route('student.index')}}">Studenti</a></li>
            <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
        </ol>
    </nav>
    <h2 class="mt-30 text-center">Editeaza Studentul {{$student->first_name}} {{$student->last_name}}</h2>

    <form action="{{route('student.update', ['id' => $student->id])}}" method="post">
        <div class="row mt-30">
            <div class="col-sm-4 col-sm-offset-4">

                {{ csrf_field() }}
                <label for="label" class="control-label">Nume</label>
                <input type="text" name="first_name" id="first_name" value="{{$student->first_name}}">
                <label for="label" class="control-label mt-30">Prenume</label>
                <input type="text" name="last_name" id="last_name" value="{{$student->last_name}}">
                <label for="label" class="control-label mt-30">Alegeti subgrupa</label>
                <select name="subgroup" id="subgroup" class="form-control">
                    @foreach($subgroups as $subgroup)
                        <option value="{{$subgroup->id}}" @if($student->subgroup_id == $subgroup->id) selected @endif>{{$subgroup->label}}</option>
                    @endforeach
                </select>
                <button type="submit" class="btn green pull-right mt-15">Salveaza</button>
            </div>
        </div>
    </form>

@stop