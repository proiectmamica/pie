@extends('layouts.admin')

@section('title')
    Studenti
@stop

@section('styles')
    <link href="{{asset('css/jquery.dataTables.min.css')}}" rel="stylesheet">
@stop


@section('content')

    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
        </ol>
    </nav>

    <div class="row">
        <div class="col-sm-6">
            <h1>Studenti</h1>
        </div>
        <div class="col-md-6 mt-20">
            <a href="{{route('student.create')}}" class="btn green pull-right mb-10"><i class="fa fa-plus"></i> Adauga</a>
        </div>
    </div>
    <table class="table mt-30"  id="example">
        <thead>
        <tr>
            <th>Nume</th>
            <th>Prenume</th>
            <th>Subgrupa</th>
            <th>Actiuni</th>
            <th>Detalii</th>
        </tr>
        </thead>
        <tbody>
        @foreach($students as $student)
            <tr>
                <td>{{$student->first_name}}</td>
                <td>{{$student->last_name}}</td>
                <td>{{$student->subgroup->label}}</td>
                <td>
                    <a href="{{route('student.edit', ['id' => $student->id])}}" class="btn blue"><i class="fa fa-pencil-square"></i> Editeaza</a>
                </td>
                <td>
                    Creat la: {{$student->created_at}} <br>
                    Actualizat la : {{$student->updated_at}}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


@stop


@section('scripts')
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
@stop