@extends('layouts.admin')

@section('title')
    Dashboard
@stop

@section('styles')
    <link href="{{asset('css/fileinput.min.css')}}" rel="stylesheet">

@stop

@section('content')
    <h2 class="text-center">Dashboard here</h2>
    <form action="{{route('excel.import')}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row mt-15">
            <div class="col-md-4 col-md-offset-4">
                <input id="input-b5" name="file" style="width: 100%" type="file" >

            </div>
            <div class="col-md-4 col-md-offset-4 text-center mt-15">
                <button type="submit" class="btn green">Incarca</button>
            </div>
        </div>
    </form>
    <div class="row mt-15 text-center">

        <a href="{{route('discipline_module.assign')}}" class="btn blue"><i class="fa fa-plus"></i> Atribuie profesori</a>
    </div>

@stop

@section('scripts')
    <script src="{{asset('js/fileinput.min.js')}}"></script>
    <script>

        $("#input-b5").fileinput({
            showCaption: false,
            browseLabel: "Incarca fisier Excell!",
            browseClass: "btn blue btn-block",
            allowedFileExtensions: ["xlsx"],
            showUpload: false,
            showRemove:false
        });
    </script>
@stop