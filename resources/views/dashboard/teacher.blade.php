@extends('layouts.admin')

@section('title')
    Activitati profesor
@stop

@section('content')
    <div class="row">
        @if(isset($teacher))
            <div class="col-md-12" style="text-align: center">
                <h2 >Modulele Profesorului: {{$teacher->first_name}} {{$teacher->last_name}}</h2>
                <a href="{{route('dashboard')}}" class="btn green">Inapoi la modulele mele</a>
            </div>
        @endif
        <div class="col-sm-6">
            <h1 class="mt-0">{{$week->label}} </h1>
            <h4>Perioada {{$week->start}}    {{$week->end}}</h4>
        </div>
        <div class="col-sm-6">
            <a href="#teachers" data-toggle="modal" id="another-teacher" class="btn blue pull-right">Pune prezenta la alt profesor</a>
        </div>
    </div>

    <div class="row">
        @if(!isset($teacher))
            <div class="col-sm-12 mt-30 prev-next-week">
                <a href="{{route('dashboard', ['week' => $prev ])}}" class="arrows"><i class="fa fa-arrow-left" aria-hidden="true"></i> Saptamana anterioara</a>
                <a href="{{route('dashboard', ['week' => $next ])}}" class="pull-right arrows">Saptamana urmatoare <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
            </div>
        @endif
        <div class="col-sm-3">
            <h1 class="text-center">Cursuri</h1>
            @if($courses->count() > 0)
                @foreach($courses as $course)
                    <div class="dashboard-card">

                        <h1>{{$course->disciplineModule->discipline->name}}     {{$course->disciplineModule->collegeYear->label}}</h1>
                        <div>
                            <a href="#course-{{$course->id}}"  data-toggle="modal">Pune prezente</a>
                        </div>
                    </div>

                    <div class="modal fade" id="course-{{$course->id}}" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Prezenta studentilor - {{$course->disciplineModule->collegeYear->label}}</h4>
                                    <label for="check-all-{{$course->id}}">
                                        <input type="checkbox" data-name="course-{{$course->id}}" id="check-all-{{$course->id}}" onclick="checkAll(this)">
                                        Bifeaza/Debifeaza Tot
                                    </label>
                                </div>
                                <form action="{{route('attendance.store')}}" method="post">
                                    {{ csrf_field() }}
                                    <div class="modal-body dashboard-modal" >
                                        <div class="modal-table">

                                            <table class="table student-table">
                                                <thead>
                                                <tr>
                                                    <th>Nume student</th>
                                                    <th style="text-align: center">Prezenta</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($course->disciplineModule->collegeYear->groups as $group)
                                                    @foreach($group->subgroups as $subgroup)
                                                        @foreach($subgroup->students as $student)
                                                            <tr>
                                                                <td>{{$student->first_name}} {{$student->last_name}}</td>
                                                                <td>
                                                                    <input type="checkbox" id="course-{{$course->id}}-student-{{$student->id}}" name="students[]" value="{{$student->id}}"
                                                                           @if(hasAttendance($student->id, $course->id)) checked @endif
                                                                    >
                                                                    <label for="course-{{$course->id}}-student-{{$student->id}}"><span></span></label>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endforeach
                                                @endforeach
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                    <input type="hidden" name="discipline_module_week_id" value="{{$course->id}}" />
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Inchide</button>
                                        <button type="submit" class="btn green"> Salveaza</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>

                @endforeach
            @endif
        </div>
        <div class="col-sm-3">
            <h1 class="text-center">Seminarii</h1>
            @foreach($seminaries as $seminary)
                <div class="dashboard-card">
                    <h1>{{$seminary->disciplineModule->discipline->name}}  {{$seminary->disciplineModule->group->label}}</h1>
                    <div>
                        <a href="#seminary-{{$seminary->id}}"  data-toggle="modal">Pune prezente</a>
                    </div>
                </div>

                <div class="modal fade" id="seminary-{{$seminary->id}}" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Prezenta studentilor - Grupa {{$seminary->disciplineModule->group->label}}</h4>
                                <label for="check-all-{{$seminary->id}}">
                                    <input type="checkbox" data-name="seminary-{{$seminary->id}}" id="check-all-{{$seminary->id}}" onclick="checkAll(this)">
                                    Bifeaza/Debifeaza Tot
                                </label>
                            </div>
                            <form action="{{route('attendance.store')}}" method="post">
                                {{ csrf_field() }}
                                <div class="modal-body dashboard-modal" >
                                    <div class="modal-table">

                                        <table class="table student-table">
                                            <thead>
                                            <tr>
                                                <th>Nume student</th>
                                                <th style="text-align: center">Prezenta</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($seminary->disciplineModule->group->subgroups as $subgroup)
                                                @foreach($subgroup->students as $student)
                                                    <tr>
                                                        <td>{{$student->first_name}} {{$student->last_name}}</td>
                                                        <td>
                                                            <input type="checkbox" id="seminary-{{$seminary->id}}-student-{{$student->id}}" name="students[]" value="{{$student->id}}"
                                                                   @if(hasAttendance($student->id, $seminary->id)) checked @endif>
                                                            <label for="seminary-{{$seminary->id}}-student-{{$student->id}}"><span></span></label>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <input type="hidden" name="discipline_module_week_id" value="{{$seminary->id}}" />
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Inchide</button>
                                    <button type="submit" class="btn green"> Salveaza</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            @endforeach
        </div>

        <div class="col-sm-3">
            <h1 class="text-center">Laboratoare</h1>
            @foreach($laboratories as $laboratory)
                <div class="dashboard-card">
                    <h1>{{$laboratory->disciplineModule->discipline->name}} {{$laboratory->disciplineModule->subgroup->label}}</h1>
                    <div>
                        <a href="#laboratory-{{$laboratory->id}}" id="laboratory-link-{{$laboratory->id}}"  data-toggle="modal">Pune prezente</a>
                    </div>
                </div>

                <div class="modal fade" id="laboratory-{{$laboratory->id}}" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Prezenta studentilor - Subgrupa {{$laboratory->disciplineModule->subgroup->label}}</h4>
                                <label for="check-all-{{$laboratory->id}}">
                                    <input type="checkbox" data-name="laboratory-{{$laboratory->id}}" id="check-all-{{$laboratory->id}}" onclick="checkAll(this)">
                                    Bifeaza/Debifeaza Tot
                                </label>
                            </div>
                            <form action="{{route('attendance.store')}}" method="post">
                                {{ csrf_field() }}
                                <div class="modal-body dashboard-modal" >
                                    <div class="modal-table">

                                        <table class="table student-table">
                                            <thead>
                                            <tr>
                                                <th>Nume student</th>
                                                <th style="text-align: center">Prezenta</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($laboratory->disciplineModule->subgroup->students as $student)
                                                <tr>
                                                    <td>{{$student->first_name}} {{$student->last_name}}</td>
                                                    <td>
                                                        <input type="checkbox" id="laboratory-{{$laboratory->id}}-student-{{$student->id}}" name="students[]" value="{{$student->id}}"
                                                               @if(hasAttendance($student->id, $laboratory->id)) checked @endif>
                                                        <label for=laboratory-{{$laboratory->id}}-student-{{$student->id}}><span></span></label>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <input type="hidden" name="discipline_module_week_id" value="{{$laboratory->id}}" />
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Inchide</button>
                                    <button type="submit" class="btn green"> Salveaza</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>

            @endforeach
        </div>
        <div class="col-sm-3">
            <h1 class="text-center">Proiecte</h1>
            @foreach($projects as $project)
                <div class="dashboard-card">
                    <h1>{{$project->disciplineModule->discipline->name}} {{$project->disciplineModule->group->label}}</h1>
                    <div>
                        <a href="#project-{{$project->id}}"  data-toggle="modal">Pune prezente</a>
                    </div>
                </div>

                <div class="modal fade" id="project-{{$project->id}}" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Prezenta studentilor - Grupa {{$project->disciplineModule->group->label}}</h4>
                                <label for="check-all-{{$project->id}}">
                                    <input type="checkbox" data-name="project-{{$project->id}}" id="check-all-{{$project->id}}" onclick="checkAll(this)">
                                    Bifeaza/Debifeaza Tot
                                </label>
                            </div>
                            <form action="{{route('attendance.store')}}" method="post">
                                {{ csrf_field() }}
                                <div class="modal-body dashboard-modal" >
                                    <div class="modal-table">

                                        <table class="table student-table">
                                            <thead>
                                            <tr>
                                                <th>Nume student</th>
                                                <th style="text-align: center">Prezenta</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($project->disciplineModule->group->subgroups as $subgroup)
                                                @foreach($subgroup->students as $student)
                                                    <tr>
                                                        <td>{{$student->first_name}} {{$student->last_name}}</td>
                                                        <td>
                                                            <input type="checkbox" id="project-{{$project->id}}-student-{{$student->id}}" name="students[]" value="{{$student->id}}"
                                                                   @if(hasAttendance($student->id, $project->id)) checked @endif>
                                                            <label for="project-{{$project->id}}-student-{{$student->id}}"><span></span></label>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <input type="hidden" name="discipline_module_week_id" value="{{$project->id}}" />
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Inchide</button>
                                    <button type="submit" class="btn green"> Salveaza</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <div class="modal fade" id="teachers" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Alege profesorul si saptamana</h4>
                </div>
                <form action="{{route('dashboard')}}" method="get">
                    <div class="modal-body" >
                        <label for="teacher" class="control-label">Alege Profesorul</label>
                        <select name="teacher" id="teacher" class="form-control">
                            @foreach($teachers as $teacher)
                                <option value="{{$teacher->id}}">{{$teacher->first_name}} {{$teacher->last_name}}</option>
                            @endforeach
                        </select>
                        <label for="week" class="control-label mt-30">Alege Saptamana</label>
                        <select name="week" id="week" class="form-control">
                            @foreach($weeks as $newWeek)
                                <option value="{{$newWeek->id}}" @if($newWeek->id == $week->id) selected @endif>@if($newWeek->term_id == 1) Semestrul I @else Semestrul II @endif - {{$newWeek->label}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Inchide</button>
                        <button type="submit" id="change-teacher" class="btn green"> Salveaza</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
@stop

@section('scripts')
    <script>
        function checkAll(input) {
            var id = $(input).attr('data-name');
            $.each($(`[id^=${id}]`), function (index, value) {
                $(value).prop('checked', $(input).prop("checked"));
            })
        }
    </script>
@stop