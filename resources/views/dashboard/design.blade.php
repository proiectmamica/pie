@extends('layouts.admin')

@section('content')
    <h2>Dashboard here</h2>

    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item"><a href="#">Library</a></li>
            <li class="breadcrumb-item active" aria-current="page">Data</li>
        </ol>
    </nav>

    <div class="row">
        <div class="col-sm-4">
            <div class="dashboard-card">
                <h1>Profesori</h1>
                <p>Lorem ipsum dolor sit amet consectetur adipiscing.</p>
                <div>
                    <a href="">Acceseaza</a>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="dashboard-card">
                <h1>Studenti</h1>
                <p>Lorem ipsum dolor sit amet consectetur adipiscing.</p>
                <div>
                    <a href="">Acceseaza</a>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="dashboard-card">
                <h1>Grupe</h1>
                <p>Lorem ipsum dolor sit amet consectetur adipiscing.</p>
                <div>
                    <a href="">Acceseaza</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-15">
        <div class="col-sm-4">
            <h1>Butoane</h1>
            <button class="btn ">Default</button>
            <button class="btn blue">Blue</button>
            <button class="btn turquoise">Turquoise</button>
            <button class="btn green">Green</button>
            <button id="myBtn" class="btn">Modal!</button>
        </div>
        <div class="col-sm-4">
            <h1>Formular</h1>
            <input />
            <div class="mt-15"></div>
            <select>
                <option>Option 1</option>
                <option>Option 2</option>
                <option>Option 3</option>
            </select>
            <textarea class="mt-15" rows="3" placeholder="Mesajul dvs..."></textarea>
            <div class="mt-15">
                <ul class="unstyled centered">
                    <li>
                        <input class="styled-checkbox" id="styled-checkbox-1" type="checkbox" value="value1">
                        <label for="styled-checkbox-1">Checkbox</label>
                    </li>
                    <li>
                        <input class="styled-checkbox" id="styled-checkbox-2" type="checkbox" value="value2" checked>
                        <label for="styled-checkbox-2">CSS Only</label>
                    </li>
                    <li>
                        <input class="styled-checkbox" id="styled-checkbox-3" type="checkbox" value="value3" disabled>
                        <label for="styled-checkbox-3">A disabled checkbox</label>
                    </li>
                    <li>
                        <input class="styled-checkbox" id="styled-checkbox-4" type="checkbox" value="value4">
                        <label for="styled-checkbox-4">Fourth option</label>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-sm-4">
            <h1>Tabel</h1>
            <table class="table">
                <thead>
                <tr>
                    <th>Firstname</th>
                    <th>Lastname</th>
                    <th>Email</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>John</td>
                    <td>Doe</td>
                    <td>john@example.com</td>
                </tr>
                <tr>
                    <td>Mary</td>
                    <td>Moe</td>
                    <td>mary@example.com</td>
                </tr>
                <tr>
                    <td>July</td>
                    <td>Dooley</td>
                    <td>july@example.com</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <!-- The Modal -->
    <div id="myModal" class="modal">
        <!-- Modal content -->
        <div class="modal-content">
            <span class="close">&times;</span>

            <h1 class="school-book mt-0 mb-30 green-text">Grupa 22C31A</h1>
            <div class="modal-table">
                <table class="table student-table">
                    <thead>
                    <tr>
                        <th>Nume student</th>
                        <th style="text-align: center">Prezenta</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>John Elton</td>
                        <td>
                            <input type="checkbox" id="student1" name="student" value="value1">
                            <label for="student1"><span></span></label>
                        </td>
                    </tr>
                    <tr>
                        <td>Mary Anne</td>
                        <td>
                            <input type="checkbox" id="student2" name="student" value="value2">
                            <label for="student2"><span></span></label>
                        </td>
                    </tr>
                    <tr>
                        <td>July Marianne</td>
                        <td>
                            <input type="checkbox" id="student3" name="student" value="value3">
                            <label for="student3"><span></span></label>
                        </td>
                    </tr>
                    <tr>
                        <td>John Elton</td>
                        <td>
                            <input type="checkbox" id="student4" name="student" value="value4">
                            <label for="student4"><span></span></label>
                        </td>
                    </tr>
                    <tr>
                        <td>Mary Anne</td>
                        <td>
                            <input type="checkbox" id="student5" name="student" value="value5">
                            <label for="student5"><span></span></label>
                        </td>
                    </tr>
                    <tr>
                        <td>July Marianne</td>
                        <td>
                            <input type="checkbox" id="student6" name="student" value="value6">
                            <label for="student6"><span></span></label>
                        </td>
                    </tr>
                    <tr>
                        <td>John Elton</td>
                        <td>
                            <input type="checkbox" id="student7" name="student" value="value7">
                            <label for="student7"><span></span></label>
                        </td>
                    </tr>
                    <tr>
                        <td>Mary Anne</td>
                        <td>
                            <input type="checkbox" id="student8" name="student" value="value8">
                            <label for="student8"><span></span></label>
                        </td>
                    </tr>
                    <tr>
                        <td>July Marianne</td>
                        <td>
                            <input type="checkbox" id="student9" name="student" value="value9">
                            <label for="student9"><span></span></label>
                        </td>
                    </tr>
                    <tr>
                        <td>John Elton</td>
                        <td>
                            <input type="checkbox" id="student10" name="student" value="value10">
                            <label for="student10"><span></span></label>
                        </td>
                    </tr>
                    <tr>
                        <td>Mary Anne</td>
                        <td>
                            <input type="checkbox" id="student11" name="student" value="value11">
                            <label for="student11"><span></span></label>
                        </td>
                    </tr>
                    <tr>
                        <td>July Marianne</td>
                        <td>
                            <input type="checkbox" id="student12" name="student" value="value12">
                            <label for="student12"><span></span></label>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-buttons-section">
                <button type="reset" class="btn">Cancel</button>
                <button type="submit" class="btn green">Submit</button>
            </div>
        </div>
    </div>
@stop