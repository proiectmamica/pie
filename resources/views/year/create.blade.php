@extends('layouts.admin')

@section('title')
    Adauga An
@stop


@section('content')
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{route('year.index')}}">Ani</a></li>
            <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
        </ol>
    </nav>
    <h2 class="text-center">Adauga an</h2>

    <form action="{{route('year.store')}}" method="post">
        <div class="row mt-30">
            <div class="col-md-4 col-md-offset-4">

                {{ csrf_field() }}
                <label for="label" class="control-label">Denumire</label>
                <input type="text" name="label" id="label" required>
                <label for="start" class="control-label">Data de incepere</label>
                <input type="date" id="start" name="start" class="datepicker">
                <label for="start" class="control-label">Data de sfarsit</label>
                <input type="date" id="start" name="end" class="datepicker">
            </div>
            <div class="col-md-4 col-md-offset-4 text-center">
                <button type="submit" class="btn green pull-right mt-15">Salveaza</button>
            </div>
        </div>
    </form>

@stop
