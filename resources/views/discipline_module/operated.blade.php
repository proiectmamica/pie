@extends('layouts.admin')

@section('title')
    Module operate - Prezenta studentilor
@stop

@section('content')

    <div class="row">

            <div class="col-sm-3">
                @foreach($courses as $course)
                    <div class="dashboard-card">
                        <h1>{{$course->week->label}}</h1>
                        <h1> {{$course->disciplineModule->module->label}}   {{$course->disciplineModule->discipline->name}}</h1>
                        <h1>{{$course->disciplineModule->collegeYear->label}}</h1>
                        <div>
                            <a href="#course-{{$course->id}}"  id="course-link-{{$course->id}}" data-toggle="modal">Vezi prezente</a>
                        </div>
                    </div>

                    <div class="modal fade" id="course-{{$course->id}}" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Prezenta studentilor - {{$course->disciplineModule->collegeYear->label}}</h4>
                                </div>
                                <form action="{{route('attendance.store')}}" method="post">
                                    {{ csrf_field() }}
                                    <div class="modal-body dashboard-modal" >
                                        <div class="modal-table">

                                            <table class="table student-table">
                                                <thead>
                                                <tr>
                                                    <th>Nume student</th>
                                                    <th style="text-align: center">Prezenta</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($course->disciplineModule->collegeYear->groups as $group)
                                                    @foreach($group->subgroups as $subgroup)
                                                        @foreach($subgroup->students as $student)
                                                            <tr>
                                                                <td>{{$student->first_name}} {{$student->last_name}}</td>
                                                                <td>
                                                                    <input type="checkbox" id="course-{{$course->id}}-student-{{$student->id}}" name="students[]" value="{{$student->id}}"
                                                                           @if (hasAttendance($student->id, $course->id)) checked @endif
                                                                    />
                                                                    <label for="course-{{$course->id}}-student-{{$student->id}}"><span></span></label>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    @endforeach
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <input type="hidden" name="discipline_module_week_id" value="{{$course->id}}" />
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn green"> Salveaza</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>

                @endforeach
            </div>
            <div class="col-sm-3">
                @foreach($seminaries as $seminary)
                    <div class="dashboard-card">
                        <h1>{{$seminary->week->label}}</h1>
                        <h1>{{$seminary->disciplineModule->module->label}}  {{$seminary->disciplineModule->discipline->name}} </h1>
                        <h1>Grupa {{$seminary->disciplineModule->group->label}}</h1>
                        <div>
                            <a href="#seminary-{{$seminary->id}}"  data-toggle="modal">Vezi prezente</a>
                        </div>
                    </div>

                    <div class="modal fade" id="seminary-{{$seminary->id}}" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Prezenta studentilor - Grupa {{$seminary->disciplineModule->group->label}}</h4>
                                </div>
                                <form action="{{route('attendance.store')}}" method="post">
                                    {{ csrf_field() }}
                                    <div class="modal-body dashboard-modal" >
                                        <div class="modal-table">

                                            <table class="table student-table">
                                                <thead>
                                                <tr>
                                                    <th>Nume student</th>
                                                    <th style="text-align: center">Prezenta</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($seminary->disciplineModule->group->subgroups as $subgroup)
                                                    @foreach($subgroup->students as $student)
                                                        <tr>
                                                            <td>{{$student->first_name}} {{$student->last_name}}</td>
                                                            <td>
                                                                <input type="checkbox" id="seminary-{{$seminary->id}}-student-{{$student->id}}" name="students[]" value="{{$student->id}}"
                                                                       @if (hasAttendance($student->id, $seminary->id)) checked @endif
                                                                >
                                                                <label for="seminary-{{$seminary->id}}-student-{{$student->id}}"><span></span></label>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <input type="hidden" name="discipline_module_week_id" value="{{$seminary->id}}" />
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn green"> Salveaza</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                @endforeach
            </div>

            <div class="col-sm-3">
                @foreach($laboratories as $laboratory)
                    <div class="dashboard-card">
                        <h1>{{$laboratory->week->label}}</h1>
                        <h1>{{$laboratory->disciplineModule->module->label}}  {{$laboratory->disciplineModule->discipline->name}}</h1>
                        <h1>Subgrupa {{$laboratory->disciplineModule->subgroup->label}}</h1>
                        <div>
                            <a href="#laboratory-{{$laboratory->id}}"  data-toggle="modal">Vezi prezente</a>
                        </div>
                    </div>

                    <div class="modal fade" id="laboratory-{{$laboratory->id}}" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Prezenta studentilor - Subgrupa {{$laboratory->disciplineModule->subgroup->label}}</h4>
                                </div>
                                <form action="{{route('attendance.store')}}" method="post">
                                    {{ csrf_field() }}
                                    <div class="modal-body dashboard-modal" >
                                        <div class="modal-table">

                                            <table class="table student-table">
                                                <thead>
                                                <tr>
                                                    <th>Nume student</th>
                                                    <th style="text-align: center">Prezenta</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($laboratory->disciplineModule->subgroup->students as $student)
                                                    <tr>
                                                        <td>{{$student->first_name}} {{$student->last_name}}</td>
                                                        <td>
                                                            <input type="checkbox" id="laboratory-{{$laboratory->id}}-student-{{$student->id}}" name="students[]" value="{{$student->id}}"
                                                                   @if (hasAttendance($student->id, $laboratory->id)) checked @endif
                                                            >
                                                            <label for=laboratory-{{$laboratory->id}}-student-{{$student->id}}><span></span></label>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <input type="hidden" name="discipline_module_week_id" value="{{$laboratory->id}}" />
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn green"> Salveaza</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>

                @endforeach
            </div>
            <div class="col-sm-3">
                @foreach($projects as $project)
                    <div class="dashboard-card">
                        <h1>{{$project->week->label}}</h1>
                        <h1>{{$project->disciplineModule->module->label}} {{$project->disciplineModule->discipline->name}}</h1>
                        <h1>Grupa {{$project->disciplineModule->group->label}}</h1>
                        <div>
                            <a href="#project-{{$project->id}}"  data-toggle="modal">Vezi prezente</a>
                        </div>
                    </div>

                    <div class="modal fade" id="project-{{$project->id}}" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Prezenta studentilor - Grupa {{$project->disciplineModule->group->label}}</h4>
                                </div>
                                <form action="{{route('attendance.store')}}" method="post">
                                    {{ csrf_field() }}
                                    <div class="modal-body dashboard-modal" >
                                        <div class="modal-table">

                                            <table class="table student-table">
                                                <thead>
                                                <tr>
                                                    <th>Nume student</th>
                                                    <th style="text-align: center">Prezenta</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($project->disciplineModule->group->subgroups as $subgroup)
                                                    @foreach($subgroup->students as $student)
                                                        <tr>
                                                            <td>{{$student->first_name}} {{$student->last_name}}</td>
                                                            <td>
                                                                <input type="checkbox" id="project-{{$project->id}}-student-{{$student->id}}" name="students[]" value="{{$student->id}}"
                                                                       @if (hasAttendance($student->id, $project->id)) checked @endif
                                                                >
                                                                <label for="project-{{$project->id}}-student-{{$student->id}}"><span></span></label>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <input type="hidden" name="discipline_module_week_id" value="{{$project->id}}" />
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn green"> Salveaza</button>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                @endforeach
            </div>
        </div>

    </div>
@stop