@extends('layouts.admin')

@section('title')
    Adauga Profesor Disciplina Modul
@stop


@section('content')
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{route('discipline_module.index')}}">Module Discipline</a></li>
            <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
        </ol>
    </nav>
    <h2 class="mt-30 text-center">Adauga Profesor Disciplina Modul</h2>

    <form action="{{route('discipline_module.store')}}" method="post">
        <div class="row mt-30">
            <div class="col-sm-4 col-sm-offset-4">

                {{ csrf_field() }}
                <label for="discipline" class="control-label">Disciplina</label>

                <select name="discipline" id="discipline" class="form-control">
                    @foreach($disciplines as $discipline)
                        <option value="{{$discipline->id}}">{{$discipline->name}}</option>
                    @endforeach
                </select>
                <label for="teacher" class="control-label mt-30">Profesor</label>

                <select name="teacher" id="teacher" class="form-control">
                    @foreach($teachers as $teacher)
                        <option value="{{$teacher->id}}">{{$teacher->first_name}} {{$teacher->last_name}}</option>
                    @endforeach
                </select>
                <label for="module" class="control-label mt-30">Modul</label>

                <select name="module" id="module" class="form-control">
                    @foreach($modules as $module)
                        <option value="{{$module->id}}">{{$module->label}}</option>
                    @endforeach
                </select>
                <label for="college_year" class="control-label mt-30">An facultate</label>

                <select name="college_year" id="college_year" class="form-control">
                    @foreach($college_years as $college_year)
                        <option value="{{$college_year->id}}">{{$college_year->label}}</option>
                    @endforeach
                </select>
                <label for="group" class="control-label mt-30">Grupa</label>

                <select name="group" id="group" class="form-control">
                    @foreach($groups as $group)
                        <option value="{{$group->id}}">{{$group->label}}</option>
                    @endforeach
                </select>
                <label for="subgroup" class="control-label mt-30">Subgrupa</label>

                <select name="subgroup" id="subgroup" class="form-control">
                    @foreach($subgroups as $subgroup)
                        <option value="{{$subgroup->id}}">{{$subgroup->label}}</option>
                    @endforeach
                </select>
                <label for="rotation" class="control-label mt-30">Rotatie</label>

                <select name="rotation" id="rotation" class="form-control">
                    @foreach($rotations as $rotation)
                        <option value="{{$rotation->id}}">{{$rotation->label}}</option>
                    @endforeach
                </select>

                <button type="submit" class="btn green pull-right mt-15">Salveaza</button>
            </div>
        </div>
    </form>

@stop
