@extends('layouts.admin')

@section('title')
    Editeaza Modulul Disciplinei {{$discipline_module->hours}} {{$discipline_module->modulable_type}} {{$discipline_module->modulable_id}}
@stop


@section('content')

    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{route('discipline_module.index')}}">Module Discipline</a></li>
            <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
        </ol>
    </nav>
    <h2 class="text-center mt-30">Editeaza Modulul Disciplinei {{$discipline_module->hours}} {{$discipline_module->modulable_type}} {{$discipline_module->modulable_id}}</h2>

    <form action="{{route('discipline_module.update', ['id' => $discipline_module->id])}}" method="post">
        <div class="row mt-30">
            <div class="col-sm-4 col-sm-offset-4">

                {{ csrf_field() }}
                <label for="discipline" class="control-label">Disciplina</label>
                <select name="discipline" id="discipline" class="form-control">
                    @foreach($disciplines as $discipline)
                        <option value="{{$discipline->id}}" @if($discipline->id == $discipline_module->discipline_id) selected @endif> {{$discipline->name}}</option>
                    @endforeach
                </select>
                <label for="teacher" class="control-label mt-30">Profesor</label>
                <select name="teacher" id="teacher" class="form-control" required>
                    <option  selected disabled>Alege Profesorul</option>
                    @foreach($teachers as $teacher)
                        <option value="{{$teacher->id}}" @if(isset($discipline_module->teacher_id) && $teacher->id == $discipline_module->teacher_id) selected @endif> {{$teacher->first_name}} {{$teacher->last_name}}</option>
                    @endforeach
                </select>
                <label for="module" class="control-label mt-30">Modul</label>
                <select name="module" id="module" class="form-control">
                    @foreach($modules as $module)
                        <option value="{{$module->id}}" @if(isset($discipline_module->module_id) && $module->id == $discipline_module->module_id) selected @endif> {{$module->label}}</option>
                    @endforeach
                </select>
                <label for="college_year" class="control-label mt-30">An facultate</label>
                <select name="college_year" id="college_year" class="form-control" required>
                    <option selected disabled>Alege anul de facultate</option>
                    @foreach($college_years as $college_year)
                        <option value="{{$college_year->id}}" @if(isset($discipline_module->college_year_id) && $college_year->id == $discipline_module->college_year_id) selected @endif> {{$college_year->label}}</option>
                    @endforeach
                </select>
                <label for="group" class="control-label mt-30">Grupa</label>
                <select name="group" id="group" class="form-control" required>
                    <option selected disabled>Alege grupa</option>
                    @foreach($groups as $group)
                        <option value="{{$group->id}}" @if(isset($discipline_module->group_id) && $group->id == $discipline_module->group_id) selected @endif> {{$group->label}}</option>
                    @endforeach
                </select>
                <label for="subgroup" class="control-label mt-30">Subgrupa</label>
                <select name="subgroup" id="subgroup" class="form-control" required>
                    <option selected disabled>Alege subgrupa</option>
                    @foreach($subgroups as $subgroup)
                        <option value="{{$subgroup->id}}" @if(isset($discipline_module->subgroup_id) && $subgroup->id == $discipline_module->subgroup_id) selected @endif> {{$subgroup->label}}</option>
                    @endforeach
                </select>
                <label for="rotation" class="control-label mt-30">Rotatie</label>
                <select name="rotation" id="rotation" class="form-control" required>
                    <option selected disabled>Alege rotatia</option>
                    @foreach($rotations as $rotation)
                        <option value="{{$rotation->id}}" @if(isset($discipline_module->rotation_id) && $rotation->id == $discipline_module->rotation_id) selected @endif> {{$rotation->label}}</option>
                    @endforeach
                </select>
                <button type="submit" class="btn green pull-right mt-15">Salveaza</button>
            </div>
        </div>
    </form>

@stop
