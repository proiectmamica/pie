@extends('layouts.admin')

@section('title')
    Atribuie profesori
@stop

@section('content')

    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{route('discipline_module.index')}}">Module Discipline</a></li>
            <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
        </ol>
    </nav>

    <h1 class="mt-30 text-center mb-30">Atribuie profesori</h1>

    <form id="assign-form" action="{{route('discipline_module.save.assign')}}" method="post">
        {{ csrf_field() }}
        <div class="row mt-30">

            <div class="col-md-9">
                <div class="assign-box">
                    <h3>Module Discipline</h3>

                    <div class="content lionbar mCustomScrollbar" data-mcs-theme="minimal-dark">
                        @foreach($disciplineModules as $disciplineModule)
                            <div id="div-discipline-module-{{$disciplineModule->id}}" class="row">
                                <div class="col-md-4">
                                    <label for="discipline-module-{{$disciplineModule->id}}" class="control-label">
                                        <input  type="checkbox" name="discipline_modules[]" id="discipline-module-{{$disciplineModule->id}}" value="{{$disciplineModule->id}}" style="width: auto"/>
                                        {{$disciplineModule->discipline->name}} -
                                        {{$disciplineModule->module->label}} -
                                        @if(isset($disciplineModule->collegeYear))
                                            {{$disciplineModule->collegeYear->label}}
                                        @endif
                                        @if(isset($disciplineModule->group))
                                            {{$disciplineModule->group->label}}
                                        @endif
                                        @if(isset($disciplineModule->subgroup))
                                            {{$disciplineModule->subgroup->label}}
                                        @endif
                                    </label>
                                </div>
                                <div class="col-md-8">
                                    @if(
                                    ($disciplineModule->module->id == 1 && $disciplineModule->discipline->number_hours_course % 2 == 0) ||
                                    ($disciplineModule->module->id == 2 && $disciplineModule->discipline->number_hours_laboratory % 2 == 0) ||
                                    ($disciplineModule->module->id == 3 && $disciplineModule->discipline->number_hours_seminary % 2 == 0) ||
                                    ($disciplineModule->module->id == 4 && $disciplineModule->discipline->number_hours_project % 2 == 0)
                                     )
                                        <label for="rotation-{{$disciplineModule->id}}" class="control-label">
                                            <input type="radio" name="rotation-{{$disciplineModule->id}}" checked id="rotation-{{$disciplineModule->id}}" value="1" style="width: auto;display: inline-block">
                                            In Fiecare saptamana
                                        </label>

                                    @else
                                        @foreach($rotations as $rotation)
                                            @if($rotation->id != 1)
                                                <label for="rotation-{{$disciplineModule->id}}-{{$rotation->id}}" class="control-label">
                                                    <input type="radio" name="rotation-{{$disciplineModule->id}}" id="rotation-{{$disciplineModule->id}}-{{$rotation->id}}" value="{{$rotation->id}}" style="width: auto;display: inline-block">
                                                    {{$rotation->label}}
                                                </label>
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                                <br>
                            </div>
                        @endforeach
                    </div>
                    <button class="btn  blue" id="unselect" type="button">Deselecteaza tot</button>
                </div>
            </div>
            <div class="col-md-3">
                <div class="assign-box">
                    <h3>Profesori</h3>

                    <div class="content lionbar mCustomScrollbar" data-mcs-theme="minimal-dark">

                        @foreach($teachers as $teacher)
                            <div id="div-teacher-{{$teacher->id}}">
                                <label for="teacher-{{$teacher->id}}" class="control-label">
                                    <input type="radio" name="teacher" id="teacher-{{$teacher->id}}" value="{{$teacher->id}}" style="width: auto">
                                    {{$teacher->last_name}} {{$teacher->first_name}}
                                </label> <br>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>


        </div>
        <div class="row mt-30 text-center">
            <div class="col-sm-4 col-sm-offset-4">
                <button type="submit" id="btn" class="btn green full-width">Salveaza</button>
            </div>
        </div>
    </form>

@stop

@section('scripts')
    <script src="{{asset('js\sweetalert2.all.js')}}"></script>

    <script>


        $('#assign-form').on('submit', function (e) {
            e.preventDefault();
            var fewSeconds = 2;
            // Ajax request
            var btn = $('#btn');
            btn.prop('disabled', true);
            setTimeout(function(){
                btn.prop('disabled', false);
            }, fewSeconds*1000);

            if ($('[id^="discipline-module"]:checked').length == 0){
                toastr["error"]("Va rugam sa alegeti modulul disciplinei!");
                return 0;
            }

            let disciplines = '';
            var verification = true;
            $.each($('[id^="discipline-module"]:checked'), function (index, value) {
                let id = $(this).attr('id')
                disciplines +=  $(`label[for='${id}']`).text().replace(/\s/g, "") + " , ";
                var array = $(this).attr('id').split("-");
                if(!$(`input[name="rotation-${array[2]}"]`).is(':checked')) {
                    verification = false;
                    toastr["error"]("Va rugam sa alegeti rotatie pentru toate disciplinele alese!");
                }
                return 0;
            })

            disciplines = disciplines.slice(0, -2);
            if(!$('input[name=teacher]').is(':checked'))
            {
                toastr["error"]("Va rugam sa alegeti un profesor!");
                return 0;
            }


            if (verification == true) {
                let teacherId = $('input[name=teacher]:checked').attr('id');
                let teacherName = $(`label[for='${teacherId}']`).text();

                let text = `Doriti sa atribuiti profesorului ${teacherName} materiile: ${disciplines}`;
                swal({
                    type: 'info',
                    title: text,
                    showConfirmButton: true,
                    showCancelButton: true,
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url: $('#assign-form').attr("action"),
                            method: "post",
                            data: $('#assign-form').serialize(),
                            success: function (data) {
                                console.log(data);
                                $.each(data, function (index, value) {
                                    $(`#div-discipline-module-${value}`).remove();
                                    toastr["success"]("Atribuire efectuata cu succes!");
                                })
                            }
                        })
                    }
                })
            }
        });

        $('#unselect').on('click', function () {
            $.each($('[id^="discipline-module"]:checked'), function (index, value) {
                $(this).click()

            })
            toastr["success"]("Discipline deselectate");
        })
    </script>
@stop