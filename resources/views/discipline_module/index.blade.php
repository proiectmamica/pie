@extends('layouts.admin')

@section('title')
    Module Disciplina
@stop


@section('styles')
    <link href="{{asset('css/jquery.dataTables.min.css')}}" rel="stylesheet">
@stop


@section('content')

    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
        </ol>
    </nav>

    <div class="row">
        <div class="col-sm-6">
            <h1>Module Disciplina</h1>
        </div>
        <div class="col-md-6 mt-20">
            <a href="{{route('discipline_module.create')}}" class="btn green pull-right mb-10"><i class="fa fa-plus"></i> Adauga</a>
            <a href="{{route('discipline_module.assign')}}" class="btn mr-15 turquoise pull-right mb-10"><i class="fa fa-plus"></i> Atribuie profesori</a>
        </div>
    </div>


    <table class="table mt-30" id="example">
        <thead>
        <tr>
            <th>Disciplina</th>
            <th>Profesor</th>
            <th>Modul</th>
            <th>Anul</th>
            <th>Grupa</th>
            <th>Subgrupa</th>
            <th>Rotatia</th>
            <th>Actiuni</th>
            <th>Detalii</th>
        </tr>
        </thead>
        <tbody>
        @foreach($discipline_modules as $discipline_module)

            <tr>
                <td>{{$discipline_module->discipline->name}}</td>
                <td>
                    @if(isset($discipline_module->teacher))
                        {{$discipline_module->teacher->first_name}} {{$discipline_module->teacher->last_name}}
                    @endif
                </td>
                <td>
                    @if(isset($discipline_module->module))
                        {{$discipline_module->module->label}}
                    @endif
                </td>
                <td>
                    @if(isset($discipline_module->collegeyear))
                        {{$discipline_module->collegeyear->label}}
                    @endif
                </td>
                <td>
                    @if(isset($discipline_module->group))
                        {{$discipline_module->group->label}}
                    @endif
                </td>
                <td>
                    @if(isset($discipline_module->subgroup))
                        {{$discipline_module->subgroup->label}}
                    @endif
                </td>
                <td>
                    @if(isset($discipline_module->rotation))
                        {{$discipline_module->rotation->label}}
                    @endif
                </td>

                <td>
                    <a href="{{route('discipline_module.edit', ['id' => $discipline_module->id])}}" class="btn blue"><i class="fa fa-pencil-square"></i> Editeaza</a>
                </td>
                <td>
                    Creat la: {{$discipline_module->created_at}} <br>
                    Actualizat la : {{$discipline_module->updated_at}}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


@stop

@section('scripts')
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
@stop