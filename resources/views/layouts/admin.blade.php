<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('css/custom.css')}}" rel="stylesheet">
    <link href="{{asset('css/toastr.css')}}" rel="stylesheet">
    <link href="{{asset('css/jquery.mCustomScrollbar.css')}}" rel="stylesheet">

    <style>
        /* Center the loader */
        #loader {
            position: absolute;
            left: 50%;
            top: 50%;
            z-index: 99999;
            margin: -75px 0 0 -75px;
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #3498db;
            width: 120px;
            height: 120px;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
        }

        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }

        /* Add animation to "page content" */
        .animate-bottom {
            position: relative;
            -webkit-animation-name: animatebottom;
            -webkit-animation-duration: 1s;
            animation-name: animatebottom;
            animation-duration: 1s
        }

        @-webkit-keyframes animatebottom {
            from { bottom:-100px; opacity:0 }
            to { bottom:0px; opacity:1 }
        }

        @keyframes animatebottom {
            from{ bottom:-100px; opacity:0 }
            to{ bottom:0; opacity:1 }
        }

        #myDiv {
            display: none;
            text-align: center;
        }
    </style>
    @yield('styles')
</head>
<body onload="myFunction()" style="margin:0;">
<div style="background: #33333382;
    width: 100%;
    height: 100%;
    position: absolute;
    left: 0;
    top: 0;
    z-index: 9999;" id="close">
    <div id="loader"></div>
</div>

@include('partials.navbar')
@include('partials.side_menu')
<div id="page-content">
    @yield('content')
</div>

<script src="{{asset('js/jquery-3.2.1.js')}}"></script>
<script src="{{asset('js/bootstrap.js')}}"></script>
<script src="{{asset('js/toastr.js')}}"></script>
<script src="{{asset("js/jquery.mCustomScrollbar.concat.min.js")}}"></script>
<script>

    @if(!is_array(session('errors')) && get_class(session('errors')) == 'Illuminate\Support\ViewErrorBag')
            {{changeSessionToNormal()}}
            @endif
            @if(session('errors'))
            @foreach(session('errors') as $notification)
        toastr["error"]("{{$notification}}", 'Eroare!');
    @endforeach
            @endif
            @if(session('warnings'))
            @foreach(session('warnings') as $notification)
        toastr["warning"]("{{$notification}}", "Atentie!");
    @endforeach
            @endif
            @if(session('successes'))
            @foreach(session('successes') as $notification)
        toastr["success"]("{{$notification}}", "Succes!");
    @endforeach
            @endif
            @if(session('infos'))
            @foreach(session('infos') as $notification)
        toastr["info"]("{{$notification}}", "Info!");
    @endforeach
            @endif

            @if(session('notifies'))
            @foreach(session('notifies') as $notification)
        toastr["info"]("{{$notification}}", "Notificare!");
    @endforeach
            @endif

            @if(session('error_fields'))
            @foreach(session('error_fields') as $notification)
        toastr["error"]("Completati campul \"{{ucfirst($notification)}}\"", "Eroare!");
    @endforeach
    @endif
</script>
@yield('scripts')
<script>
    (function($){
        $(window).on("load",function(){
            $(".lionbar").mCustomScrollbar({
                scrollButtons:{ enable: true },
                theme: "dark-2"
            });
        });
    })(jQuery);

    $(document).ready(function () {
        document.getElementById("loader").style.display = "none";
        document.getElementById("close").style.display = "none";
        document.getElementById("myDiv").style.display = "block";
    })
</script>

<script>
    var myVar;

    function myFunction() {
//        myVar = setTimeout(showPage, 3000);
    }

    function showPage() {

    }
</script>

<script src="{{asset('js/custom.js')}}"></script>
</body>
</html>