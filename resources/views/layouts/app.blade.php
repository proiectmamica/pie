<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{asset('css/toastr.css')}}" rel="stylesheet">

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{asset('js/toastr.js')}}"></script>
    <script>

        @if(!is_array(session('errors')) && get_class(session('errors')) == 'Illuminate\Support\ViewErrorBag')
                {{changeSessionToNormal()}}
                @endif
                @if(session('errors'))
                @foreach(session('errors') as $notification)
            toastr["error"]("{{$notification}}", 'Eroare!');
        @endforeach
                @endif
                @if(session('warnings'))
                @foreach(session('warnings') as $notification)
            toastr["warning"]("{{$notification}}", "Atentie!");
        @endforeach
                @endif
                @if(session('successes'))
                @foreach(session('successes') as $notification)
            toastr["success"]("{{$notification}}", "Succes!");
        @endforeach
                @endif
                @if(session('infos'))
                @foreach(session('infos') as $notification)
            toastr["info"]("{{$notification}}", "Info!");
        @endforeach
                @endif

                @if(session('notifies'))
                @foreach(session('notifies') as $notification)
            toastr["info"]("{{$notification}}", "Notificare!");
        @endforeach
                @endif

                @if(session('error_fields'))
                @foreach(session('error_fields') as $notification)
            toastr["error"]("Completati campul \"{{ucfirst($notification)}}\"", "Eroare!");
        @endforeach
        @endif
    </script>
</body>
</html>
