@extends('layouts.admin')

@section('title')
    Adauga An de Facultate
@stop


@section('content')
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{route('college_year.index')}}">Ani de facultate</a></li>
            <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
        </ol>
    </nav>
    <h2 class="text-center mt-30">Adauga An de Facultate</h2>

    <form action="{{route('college_year.store')}}" method="post">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4">

                {{ csrf_field() }}
                <label for="label" class="control-label">Denumire</label>
                <input type="text" name="label" id="label">
                <div class="mt-30"></div>
                <label for="years" class="control-label">Alegeti anul universitar</label>
                <select name="year" id="year" class="form-control">
                    @foreach($years as $year)
                        <option value="{{$year->id}}">{{$year->label}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-sm-4 col-sm-offset-4">
                <button type="submit" class="btn green pull-right mt-15">Salveaza</button>
            </div>
        </div>
    </form>

@stop
