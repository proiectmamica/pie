@extends('layouts.admin')

@section('title')
    Ani
@stop

@section('content')

    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-sm-6">
            <h1>Ani</h1>
        </div>
        <div class="col-md-6 mt-20">
            <a href="{{route('college_year.create')}}" class="btn green pull-right mb-10"><i class="fa fa-plus"></i> Adauga</a>
        </div>
    </div>
    <table class="table mt-30">
        <thead>
        <tr>
            <th>Label</th>
            <th>Anul universitar</th>
            <th>Actiuni</th>
            <th>Detalii</th>
        </tr>
        </thead>
        <tbody>
        @foreach($college_years as $college_year)
            <tr>
                <td>{{$college_year->label}}</td>
                <td>
                    <a href="{{route('college_year.edit', ['id' => $college_year->id])}}" class="btn blue"><i class="fa fa-pencil-square"></i> Editeaza</a>
                </td>
                <td>
                    Creat la: {{$college_year->created_at}} <br>
                    Actualizat la : {{$college_year->updated_at}}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


@stop