@extends('layouts.admin')

@section('title')
    Editeaza Anul {{$college_year->label}}
@stop


@section('content')
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{route('college_year.index')}}">Ani de facultate</a></li>
            <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
        </ol>
    </nav>
    <h2 class="mt-30 text-center">Editeaza Anul {{$college_year->label}}</h2>

    <form action="{{route('college_year.update', ['id' => $college_year->id])}}" method="post">
        <div class="row mt-30">
            <div class="col-sm-4 col-sm-offset-4">

                {{ csrf_field() }}
                <label for="label" class="control-label">Denumire</label>
                <input type="text" name="label" id="label" value="{{$college_year->label}}">
                <div class="mt-30"></div>

                <label for="label" class="control-label">Alegeti anul universitar</label>
                <select name="year" id="year" class="form-control">
                    @foreach($years as $year)
                        <option value="{{$year->id}}" @if($year->id == $college_year->year_id) selected @endif> {{$year->label}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-sm-4 col-sm-offset-4">
                <button type="submit" class="btn green pull-right mt-15">Salveaza</button>
            </div>
        </div>
    </form>

@stop
