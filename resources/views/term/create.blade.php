@extends('layouts.admin')

@section('title')
    Adauga Semestru
@stop


@section('content')
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{route('term.index')}}">Semestre</a></li>
            <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
        </ol>
    </nav>
    <h2 class="text-center">Adauga Semestru</h2>

    <form action="{{route('term.store')}}" method="post">
        <div class="row mt-30">
            <div class="col-sm-4 col-md-offset-4">
                @include('term.form')
                <div class="col-md-12">
                    <button type="submit" class="btn green pull-right mt-15">Salveaza</button>
                </div>
            </div>
        </div>
    </form>

@stop
