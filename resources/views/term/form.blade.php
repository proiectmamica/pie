<div class="col-md-12">

    {{ csrf_field() }}
    <label for="label" class="control-label">Denumire</label>
    <input type="text" name="label" id="label" @if(isset($term)) value="{{$term->label}}" @endif>
    <div class="mt-30"></div>
    <label for="year" class="control-label">Anul Universitar</label>
    <select name="college_year" class="form-control" id="">
        @foreach($college_years as $college_year)
            <option value="{{$college_year->id}}" @if(isset($term) && $college_year->id == $term->id) selected @endif>{{$college_year->label}}</option>
        @endforeach
    </select>

</div>
<div class="col-md-12 mt-30">
    <label for="start" class="control-label">Data de incepere</label>
    <input type="date" id="start" name="start" class="datepicker" @if(isset($term)) value="{{$term->start}}" @endif>
    <div class="mt-30"></div>
    <label for="start" class="control-label">Data de sfarsit</label>
    <input type="date" id="end" name="end" class="datepicker" @if(isset($term)) value="{{$term->end}}" @endif>
</div>