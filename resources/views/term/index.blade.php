@extends('layouts.admin')

@section('title')
    Semestre
@stop

@section('content')

    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-sm-6">
            <h1>Semestre</h1>
        </div>
        <div class="col-md-6 mt-20">

            <a href="{{route('term.create')}}" class="btn green pull-right mb-10"><i class="fa fa-plus"></i> Adauga</a>
        </div>
    </div>

    <table class="table mt-30">
        <thead>
        <tr>
            <th>Label</th>
            <th>Durata</th>
            <th>Anul</th>
            <th>Actiuni</th>
            <th>Detalii</th>
        </tr>
        </thead>
        <tbody>
        @foreach($terms as $term)
            <tr>
                <td>{{$term->label}}</td>
                <td>{{$term->start}} - {{$term->end}}</td>
                <td>
                    @if(isset($term->collegeYear))
                        {{ $term->collegeYear->label }}
                    @endif
                </td>
                <td>
                    <a href="{{route('term.edit', ['id' => $term->id])}}" class="btn blue"><i class="fa fa-pencil-square"></i> Editeaza</a>
                </td>
                <td>
                    Creat la: {{$term->created_at}} <br>
                    Actualizat la : {{$term->updated_at}}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


@stop