@extends('layouts.admin')

@section('title')
    Editeaza Subgrupa {{$subgroup->label}}
@stop


@section('content')
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{route('subgroup.index')}}">Subgrupe</a></li>
            <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
        </ol>
    </nav>
    <h2 class="mt-30 text-center">Editeaza Subgrupa {{$subgroup->label}}</h2>

    <form action="{{route('subgroup.update', ['id' => $subgroup->id])}}" method="post">
        <div class="row mt-30">
            <div class="col-sm-4 col-sm-offset-4">

                {{ csrf_field() }}
                <label for="label" class="control-label">Subgrupa</label>
                <input type="text" name="label" id="label" value="{{$subgroup->label}}">
                <label for="label" class="control-label mt-30">Alegesti grupa</label>
                <select name="group" id="" class="form-control">
                    @foreach($groups as $group)
                        <option value="{{$group->id}}" @if($group->id == $subgroup->group_id) selected @endif>{{$group->label}}</option>
                    @endforeach
                </select>

                <button type="submit" class="btn green pull-right mt-15">Salveaza</button>
            </div>
        </div>

    </form>

@stop
