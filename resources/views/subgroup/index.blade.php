@extends('layouts.admin')

@section('title')
    Subgrupe
@stop

@section('content')

    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-sm-6">
            <h1>Subgrupe</h1>
        </div>
        <div class="col-md-6 mt-20">
            <a href="{{route('subgroup.create')}}" class="btn green pull-right mb-10"><i class="fa fa-plus"></i> Adauga</a>
        </div>
    </div>
    <table class="table mt-30">
        <thead>
        <tr>
            <th>Subgrupa</th>
            <th>Grupa</th>
            <th>Actiuni</th>
            <th>Detalii</th>
        </tr>
        </thead>
        <tbody>
        @foreach($subgroups as $subgroup)
            <tr>
                <td>{{$subgroup->label}}</td>
                <td>
                    @if(isset($subgroup->group))
                        {{ $subgroup->group->label }}
                    @endif
                </td>
                <td>
                    <a href="{{route('subgroup.edit', ['id' => $subgroup->id])}}" class="btn blue"><i class="fa fa-pencil-square"></i> Editeaza</a>
                </td>
                <td>
                    Creat la: {{$subgroup->created_at}} <br>
                    Actualizat la : {{$subgroup->updated_at}}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


@stop