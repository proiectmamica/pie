@extends('layouts.admin')

@section('title')
    Grupe
@stop

@section('content')

    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
        </ol>
    </nav>
    <div class="row">
        <div class="col-sm-6">
            <h1>Grupe</h1>
        </div>
        <div class="col-md-6 mt-20">
            <a href="{{route('group.create')}}" class="btn green pull-right mb-10"><i class="fa fa-plus"></i> Adauga</a>
        </div>
    </div>
    <table class="table mt-30">
        <thead>
        <tr>
            <th>Label</th>
            <th>An licenta</th>
            <th>Actiuni</th>
            <th>Detalii</th>
        </tr>
        </thead>
        <tbody>
        @foreach($groups as $group)
            <tr>
                <td>{{$group->label}}</td>
                <td>
                    @if(isset($group->collegeYear))
                        {{ $group->collegeYear->label }}
                    @endif
                </td>
                <td>
                    <a href="{{route('group.edit', ['id' => $group->id])}}" class="btn blue"><i class="fa fa-pencil-square"></i> Editeaza</a>
                </td>
                <td>
                    Creat la: {{$group->created_at}} <br>
                    Actualizat la : {{$group->updated_at}}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


@stop