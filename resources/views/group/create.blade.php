@extends('layouts.admin')

@section('title')
    Adauga Grupa
@stop

@section('content')
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{route('group.index')}}">Grupe</a></li>
            <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
        </ol>
    </nav>
    <h2 class="mt-30 text-center">Adauga grupa</h2>

    <form action="{{route('group.store')}}" method="post">
        <div class="mt-30 row">
            <div class="col-sm-4 col-sm-offset-4">
                {{ csrf_field() }}
                <label for="label" class="control-label">Denumire</label>
                <input type="text" name="label" id="label" required>
                <div class="mt-30"></div>
                <label for="college_year" class="control-label">Alegeti anul</label>
                <select name="college_year" id="college_year" class="form-control">
                    @foreach($college_years as $college_year)
                        <option value="{{$college_year->id}}">{{$college_year->label}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-sm-4 col-sm-offset-4">
                <button type="submit" class="btn green pull-right mt-15">Salveaza</button>
            </div>
        </div>
    </form>

@stop