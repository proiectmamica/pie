@extends('layouts.admin')

@section('title')
    Editeaza Grupa {{$group->label}}
@stop

@section('content')
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{route('group.index')}}">Grupe</a></li>
            <li class="breadcrumb-item active" aria-current="page">@yield('title')</li>
        </ol>
    </nav>
    <h2 class="mt-30 text-center">Editeaza Grupa {{$group->label}}</h2>

    <form action="{{route('group.update', ['id' => $group->id])}}" method="post">
        <div class="row mt-30">
            <div class="col-sm-4 col-sm-offset-4">

                {{ csrf_field() }}
                <label for="label" class="control-label">Denumire</label>
                <input type="text" name="label" id="label" value="{{$group->label}}">
                <label for="college_year" class="control-label">Alegeti anul</label>
                <select name="college_year" id="" class="form-control">
                    @foreach($college_years as $college_year)
                        <option value="{{$college_year->id}}" @if($college_year->id == $group->college_year_id) selected @endif>{{$college_year->label}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-sm-4 col-sm-offset-4">
                <button type="submit" class="btn green pull-right mt-15">Salveaza</button>
            </div>
        </div>
    </form>

@stop