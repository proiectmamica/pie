<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();


Route::get('/design', 'HomeController@design')->name('design');

// Pentru creare controller rulati comanda:
// php artisan make:controller NumeController --resource

// Cand adaugati fisiere noi in proiect nu uiti sa le adaugati si in git
// Daca sunt rosii nu sunt adaugate in git
// Cand sunt verzi sau albastre sunt in git
// Pentru a le adauga in git comanda: git add .
// Sau click dreapta pe fisier -> Git -> Add

Route::post('/excel/import', 'HomeController@import')->name('excel.import');

// Ani - Years
Route::get('/year', 'YearController@index')->name('year.index');
Route::get('/year/create', 'YearController@create')->name('year.create');
Route::post('/year/store', 'YearController@store')->name('year.store');
Route::get('/year/edit/{id}', 'YearController@edit')->name('year.edit');
Route::post('/year/update/{id}', 'YearController@update')->name('year.update');

//Terms
Route::get('/term', 'TermController@index')->name('term.index');
Route::get('/term/create', 'TermController@create')->name('term.create');
Route::post('/term/store', 'TermController@store')->name('term.store');
Route::get('/term/edit/{id}', 'TermController@edit')->name('term.edit');
Route::post('/term/update/{id}', 'TermController@update')->name('term.update');

// College Years
Route::get('/college_year', 'CollegeYearController@index')->name('college_year.index');
Route::get('/college_year/create', 'CollegeYearController@create')->name('college_year.create');
Route::post('/college_year/store', 'CollegeYearController@store')->name('college_year.store');
Route::get('/college_year/edit/{id}', 'CollegeYearController@edit')->name('college_year.edit');
Route::post('/college_year/update/{id}', 'CollegeYearController@update')->name('college_year.update');


// Grupe - Groups
Route::get('/group', 'GroupController@index')->name('group.index');
Route::get('/group/create', 'GroupController@create')->name('group.create');
Route::post('/group/store', 'GroupController@store')->name('group.store');
Route::get('/group/edit/{id}', 'GroupController@edit')->name('group.edit');
Route::post('/group/update/{id}', 'GroupController@update')->name('group.update');

//Subgroup
Route::get('/subgroup', 'SubgroupController@index')->name('subgroup.index');
Route::get('/subgroup/create', 'SubgroupController@create')->name('subgroup.create');
Route::post('/subgroup/store', 'SubgroupController@store')->name('subgroup.store');
Route::get('/subgroup/edit/{id}', 'SubgroupController@edit')->name('subgroup.edit');
Route::post('/subgroup/update/{id}', 'SubgroupController@update')->name('subgroup.update');

// Studenti - Students
Route::get('/student', 'StudentController@index')->name('student.index');
Route::get('/student/create', 'StudentController@create')->name('student.create');
Route::post('/student/store', 'StudentController@store')->name('student.store');
Route::get('/student/edit/{id}', 'StudentController@edit')->name('student.edit');
Route::post('/student/update/{id}', 'StudentController@update')->name('student.update');

//Disciplines
Route::get('/discipline', 'DisciplineController@index')->name('discipline.index');
Route::get('/discipline/create', 'DisciplineController@create')->name('discipline.create');
Route::post('/discipline/store', 'DisciplineController@store')->name('discipline.store');
Route::get('/discipline/edit/{id}', 'DisciplineController@edit')->name('discipline.edit');
Route::post('/discipline/update/{id}', 'DisciplineController@update')->name('discipline.update');

// Module - Modules
Route::get('/module', 'ModuleController@index')->name('module.index');
Route::get('/module/create', 'ModuleController@create')->name('module.create');
Route::post('/module/store', 'ModuleController@store')->name('module.store');
Route::get('/module/edit/{id}', 'ModuleController@edit')->name('module.edit');
Route::post('/module/update/{id}', 'ModuleController@update')->name('module.update');

//DisciplineModule
Route::get('/discipline_module', 'DisciplineModuleController@index')->name('discipline_module.index');
Route::get('/discipline_module/create', 'DisciplineModuleController@create')->name('discipline_module.create');
Route::post('/discipline_module/store', 'DisciplineModuleController@store')->name('discipline_module.store');
Route::get('/discipline_module/edit/{id}', 'DisciplineModuleController@edit')->name('discipline_module.edit');
Route::post('/discipline_module/update/{id}', 'DisciplineModuleController@update')->name('discipline_module.update');
Route::get('/discipline_module/assign', 'DisciplineModuleController@assign')->name('discipline_module.assign');
Route::post('/discipline-module/assign-save', 'DisciplineModuleController@saveAssign')->name('discipline_module.save.assign');
Route::get('/discipline-module/operated', 'DisciplineModuleController@getOperatedModules')->name('discipline_module.operated');


//Teacher
Route::get('/teacher', 'TeacherController@index')->name('teacher.index');
Route::get('/teacher/create', 'TeacherController@create')->name('teacher.create');
Route::post('/teacher/store', 'TeacherController@store')->name('teacher.store');
Route::get('/teacher/edit/{id}', 'TeacherController@edit')->name('teacher.edit');
Route::post('/teacher/update/{id}', 'TeacherController@update')->name('teacher.update');

// Saptamani - Weeks
Route::get('/week', 'WeekController@index')->name('week.index');
Route::get('/week/create', 'WeekController@create')->name('week.create');
Route::post('/week/store', 'WeekController@store')->name('week.store');
Route::get('/week/edit/{id}', 'WeekController@edit')->name('week.edit');
Route::post('/week/update/{id}', 'WeekController@update')->name('week.update');

// Attendance
Route::post('/attendance/store', 'AttendanceController@store')->name('attendance.store');

//Reports
Route::get('/report', 'ReportController@index')->name('report.index');
Route::get('/report/total-attendances/{id}', 'ReportController@total')->name('report.total');
Route::get('/report/groups-attendances/{id}', 'ReportController@groups')->name('report.groups');
Route::get('/report/students-attendances/{id}', 'ReportController@students')->name('report.students');
Route::get('/report/student-attendance/{id}', 'ReportController@student')->name('report.student');
Route::get('/report/student-week-attendance/{id}', 'ReportController@weeks')->name('report.weeks');


Route::get('/{week?}', 'HomeController@index')->name('dashboard');
